var statusStore = require('./mqttStatus.js').statusStore
var settingStore = require('./mqttSettings.js').settingStore
var functionStore = require('./mqttFunctions.js').functionStore

class Axis {
    constructor(options) {
        this.internalName = options.internalName //for actual use in interface implemetation e.g. grbl axis name
        this.axisName = options.axisName //for mqtt topic
        this.axisLetter = options.axisLetter //for toString method
        this.interface = options.interface //reference of the containing interface
        this.jogMin = (options.jogMin != undefined ? options.jogMin : 0.05) //minimum absolute value of jogfactor. creates deadzone in the center

        this.basetopic = this.interface.basetopic + "/" + this.axisName
        this.moveTo = functionStore.addFunction(this.basetopic + "/moveTo", (target) => {
            options.moveTo({
                internalName: this.internalName,
                axisName: this.axisName,
                axisLetter: this.axisLetter,
                include: this.isIncluded(),
                value: target
            })
        }, this.basetopic + "/moveTo")

        this.value = statusStore.addStatus(this.basetopic + "/value", 0, undefined, this.basetopic + "/value")
        this.include = settingStore.addSetting(this.basetopic + "/include", true, undefined, this.basetopic + "/include")
        this.jogfactor = settingStore.addSetting(this.basetopic + "/jogfactor", 0, undefined, this.basetopic + "/jogfactor")
        this.toggleInclude = functionStore.addFunction(this.basetopic + "/toggleInclude", () => {
            this.include.value = !this.include.value;
        }, this.basetopic + "/toggleInclude", )
    }
    toString() {
        return ("  " + this.getValue().toFixed(2)).slice(-6) + this.axisLetter + (this.isIncluded() ? "<" : " ")
    }
    getValue() {
        return this.value.value
    }
    setValue(value) {
        this.value.update(value)
    }
    isIncluded() {
        return this.include.value
    }
    getJogFactor() {
        return this.jogfactor.value
    }
    setJogFactor(value) {
        return this.jogfactor.update(value)
    }
    getData() {
        return {
            internalName: this.internalName,
            axisName: this.axisName,
            axisLetter: this.axisLetter,
            include: this.isIncluded(),
            value: this.getValue()
        }
    }

}

module.exports = Axis
