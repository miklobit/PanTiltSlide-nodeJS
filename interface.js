const Axis = require("./axis.js")
var statusStore = require('./mqttStatus.js').statusStore
var settingStore = require('./mqttSettings.js').settingStore

const INTERFACE_BASETOPIC = require("./mqttBasetopics.js").INTERFACE_BASETOPIC

class Interface {
    //name, moveSingleAxisFunction
    constructor(options) {
        this.name = options.name
        this.basetopic = INTERFACE_BASETOPIC + "/" + options.name
        this.axes = []
        this.moveSingleAxisFunction = options.moveSingleAxisFunction
        this.createKeyframeCode = options.createKeyframeCode
        this.pause = options.pause
        this.play = options.play
        this.addToCommandBuffer = options.addToCommandBuffer
        this.status = statusStore.addStatus(this.basetopic + "/status", "Idle", undefined, this.basetopic + "/status")
        this.maxJogSpeed = settingStore.addSetting(this.basetopic + "/maxJogSpeed", options.maxJogSpeed, undefined, this.basetopic + "/maxJogSpeed")
        if (options.statusCallback != undefined) {
            console.log("add statusCallback");
            console.log(this.name);
            this.status.addCallback(options.statusCallback)
        }
    }
    addAxis(axis) {
        if (this.axes[axis.axisName] == undefined) {
            axis.interface = this
            axis.moveTo = this.moveSingleAxisFunction
            this.axes[axis.axisName] = new Axis(axis)
            return this.axes[axis.axisName]
        } else {
            console.log(this);
            console.error("AXIS ALREADY DEFINED!");
        }
    }
    getAxis(axis) {
        try {
            return this.axes[axis.axisName]
        } catch (e) {
            console.error("AXIS NOT DEFIEND!");
            console.error(e);
        }
    }
    getAxesVector() {
        var vector = {
            name: this.name,
            axes: {}
        }
        Object.entries(this.axes).forEach(([axisName, axis]) => {
            vector.axes[axis.axisName] = axis.getData()
        });
        return vector
    }
}

module.exports = Interface
