var mqtt = require("./mqttHandler.js").mqttHandler
const SETTINGS_BASETOPIC = require("./mqttBasetopics.js").SETTINGS_BASETOPIC

// Executes the callback when a new value
// is assigned over MQTT. the received value
// is passed to every local callback
// If the setting is locally updated the new value
// will also be passed to all callbacks and published
// over mqtt.

class SettingStore {
    constructor() {
        this._settings = [];
    }

    addSetting(name, value, callback, topic) {
        if (this._settings[name] == undefined) {
            this._settings[name] = new Setting(name, value, callback, topic)
            return this._settings[name]
        }
        // if (value != undefined) {
        // 	this._settings[name]._setValue(value.toString());
        // }
    }

    addCallback(name, callback) {
        this._settings[name].callbacks.push(callback)
    }

    update(name, value) {
        try {
            // console.log(this);
            this._settings[name].update(value)
        } catch (e) {
            // console.error(e);
        }
    }
}

class Setting {
    constructor(name, value, callback, topic) {
        this.name = name
        this.value = value
        this.callbacks = []

        if (topic != undefined) {
            this.basetopic = topic
        } else {
            this.basetopic = mqtt.BASETOPIC + "/" + SETTINGS_BASETOPIC + "/" + name
        }

        if (callback != undefined) {
            this.addCallback(callback)
        }
        mqtt.subscribe(this.basetopic + "/W", (value) => {
            this._setValue(value)
        })
    }
    _setValue(value) {
        // console.log("Set setting [" + this.name + "]: " + value);
        this.value = value;
        this._executeCallbacks();
        mqtt.publish(this.basetopic + "/R", this.value);
    }

    _executeCallbacks() {
        this.callbacks.forEach(callback => {
            callback(this.value)
        });
    }

    update(value) {
        if (value != this.value) {
            this._setValue(value)
        }
    }

    addCallback(callback) {
        this.callbacks.push(callback)
    }
}

var settingStore = new SettingStore();
module.exports.settingStore = settingStore;
