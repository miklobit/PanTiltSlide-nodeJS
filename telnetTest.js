var telnet = require('telnet-client');
var readline = require('readline');

var connection = new telnet();

var params = {
    host: '192.168.178.75',
    port: 23,
    timeout: 3000,
    negotiationMandatory: false
};

var stdrl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
});

var telnetReadline = undefined;

connection.on('connect', function() {
    console.log("connected");
    connection.shell((err, stream) => {
        if (err) {
            console.error(err);
            process.exit(1);
        }
        // get each keystroke, not only new lines
        // process.stdin.setRawMode(true);
        // process.stdin.setEncoding('utf8');
        // process.stdin.resume();
        // // write keystrokes to the stream as they come in
        // process.stdin.on('data', (key) => {
        //     if (key == '\u0003') {
        //         // exit on ctrl+c
        //         process.exit();
        //     }
        //     stream.write(key);
        // });

        stdrl.on('line', (line) => {
            console.log(">>" + line);
            // console.log(Buffer.from(line));
			stream.write(Buffer.from(line.trim() + '\r', 'utf-8'), err => {
                if (err) console.error(err);
            });
        })

        telnetReadline = readline.createInterface({
            input: stream
        })
        telnetReadline.on('line', (input) => {
            console.log(`Received: ${input}`);
            // console.log(Buffer.from(input));
        });
    });
})

connection.connect(params)
