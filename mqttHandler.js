const mqtt = require('mqtt')

const GLOBAL_BASETOPIC = require("./mqttBasetopics.js").GLOBAL_BASETOPIC

class MqttHandler {
    constructor() {
        this.connectCallbacks = [];
        this.subscriptionCallbacks = [];
        this.mqttClient = null
        this.host = 'localhost'
        this.BASETOPIC = GLOBAL_BASETOPIC
        this.connect()
    }

    connect() {
        this.mqttClient = mqtt.connect(this.host, {
            port: 1883,
            protocol: "mqtt"
        })
        // Mqtt error calback
        this.mqttClient.on('error', (err) => {
            console.log(err)
            this.mqttClient.end()
        })

        // Connection callback
        this.mqttClient.on('connect', () => {
            // console.log(`mqtt client connected`)
			// console.log(this.connectCallbacks);
            this.connectCallbacks.forEach((callback) => {
                callback();
            });
        })

        this.mqttClient.on('message', (topic, message) => {
            // console.log("topic: " + topic)
            // console.log("	message: " + message)
            // console.log("	this.subscriptionCallbacks[topic]: " + this.subscriptionCallbacks[topic])
            // console.log("	" + this.subscriptionCallbacks[topic] != undefined);
            if (this.subscriptionCallbacks[topic] != undefined) {
                this.subscriptionCallbacks[topic](message)
            } else {
                console.error("Callback not defined: ");
                console.error("		topic: " + topic);
                console.error("		message: " + message);
            }
        })

        this.mqttClient.on('close', () => {
            console.log(`mqtt client disconnected`)
        })
    }

    publish(topic, message) {
        // console.log("topic: ")
        // console.log(topic);
        // console.log("message: ");
        // console.log(message);
		if (typeof message != "string") {
			message = message.toString()
		}

        if (!this.mqttClient.connected) {
            console.trace("CLIENT NOT CONNECTED!!!");
			console.error("topic: >" + topic + "<");
			console.error("message: >" + message + "<");
        }
        this.mqttClient.publish(topic, message, () => {
            // console.log("published");
        })
    }

    subscribe(topic, callback) {
        // console.log("subscribed: " + topic);

        this.subscriptionCallbacks[topic] = callback
        this.mqttClient.subscribe(topic);
    }

	addConnectCallback(callback) {
		if (!this.mqttClient.connected) {
			this.connectCallbacks.push(callback)
        }else {
        	callback()
        }
	}

}

var mqttHandler = new MqttHandler()
module.exports.mqttHandler = mqttHandler
