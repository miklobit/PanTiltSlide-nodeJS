var functionStore = require('./mqttFunctions.js').functionStore
var statusStore = require('./mqttStatus.js').statusStore
var mqtt = require("./mqttHandler.js").mqttHandler
var Grbl = require("./grbl.js");
var Gui = require("./gui.js");
const SerialPort = require('serialport')
const Readline = require('@serialport/parser-readline')
const fs = require('fs')
var controller = new require("./controller.js").controller;

var telnet = require('telnet-client');
var telnetConnection = new telnet();

const KEYFRAME_FILENAME = "keyframes.json"

const KEYFRAME_EDITOR_BASETOPIC = require("./mqttBasetopics.js").KEYFRAME_EDITOR_BASETOPIC


// 000.00X000.00x000.00
// 000.00A000.00y000.00
// 000.00B000.00z000.00
// 000.00F00s00.0|00|AD

// ********************

// 000.00X<000.00x<|00|
// 000.00A<000.00y<ADD
// 000.00B<000.00z<MODE
// 000.00F<00s00.0 BUSY

const serialport = new SerialPort('/dev/ttyS0', {
    baudRate: 115200
})
serialportParser = new Readline()
serialport.pipe(serialportParser)

var telnetParams = {
    host: '192.168.178.75',
    port: 23,
    timeout: 3000,
    negotiationMandatory: false
};

telnetParser = new Readline()

// pipe stdin to grbl to send manual commands
var stdinReadline = require('readline');
var rl = stdinReadline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
});

rl.on('line', (line) => {
    console.log(">>" + line);
    serialport.write(Buffer.from(line + '\r', 'utf-8'), err => {
        if (err) console.error(err);
    });
})

class KeyframeEditor {
    constructor() {
        var self = this;
        this.interfaces = []
        this.minutes = 0;
        this.seconds = 0;
        this.tenthSeconds = 0;
        this.timeEditPosition = 1; //0:tenthSeconds; 1:seconds; 2:minutes
        this.timeEditPositionStrings = ["t", "s", "m"]
        this.maxMinutes = 10;
        this.editorString = "00:00.000";
        this.keyframes = [];
        this.keyframePointer = -1;
        this.keyframeCount = 0;
        this.allInterfacesIdleCallback = undefined
        this.mode = "";
        this.modes = {
            add: "ADD",
            empty: ""
        }

        // setup panTiltSlider
        this.interfaces["panTiltSlider"] = new Grbl({
            writeFunction: (data) => {
                serialport.write(data, err => {
                    if (err) console.log(err);
                });
            },
            name: "panTiltSlider",
            rxBufferSpace: 128,
            jogIncrementTime: 0.008,
            statusInterval: 20,
            sendBufferInterval: 5,
            maxJogSpeed: 1000,
            statusCallback: (newStatus) => {
                if (this.allInterfacesIdleCallback != undefined) {
                    if (this.allInterfacesAreIdle()) {
                        this.allInterfacesIdleCallback();
                    }
                }
            },
            positionUpdateCallback: () => {
                self.gui.draw()
            },
            axes: [{
                axisName: "A",
                axisLetter: "A",
                internalName: "X"
            }, {
                axisName: "B",
                axisLetter: "B",
                internalName: "Y"
            }, {
                axisName: "X",
                axisLetter: "X",
                internalName: "Z"
            }, ]
        });

        // serial setup
        serialportParser.on('data', line => {
            this.interfaces["panTiltSlider"].read(line)
        })

        // telnet setup
        telnetConnection.on('connect', function() {
            console.log("Telnet connected");
            // console.log(telnetConnection);
            telnetConnection.shell((err, stream) => {
                if (err) {
                    console.error("telnet connection error");
                    console.error(err);
                }
                stream.pipe(telnetParser);
                self.interfaces["rotaryPlate"] = new Grbl({
                    writeFunction: (data) => {
                        stream.write(data, err => {
                            if (err) console.log(err);
                        });
                    },
                    name: "rotaryPlate",
                    rxBufferSpace: 1200,
                    jogIncrementTime: 0.020,
                    statusInterval: 40,
                    sendBufferInterval: 10,
                    maxJogSpeed: 10000,
                    statusCallback: (newStatus) => {
                        if (self.allInterfacesIdleCallback != undefined) {
                            if (self.allInterfacesAreIdle()) {
                                self.allInterfacesIdleCallback();
                            }
                        }
                    },
                    positionUpdateCallback: () => {
                        self.gui.draw()
                    },
                    axes: [{
                        axisName: "C",
                        axisLetter: "C",
                        internalName: "X"
                    }]
                });

                self.settingsPage.addNumber({
                    name: "Jog Rotary",
                    value: self.interfaces.rotaryPlate.maxJogSpeed.value,
                    drawCallback: () => {
                        return self.interfaces.rotaryPlate.maxJogSpeed.value
                    },
                    callback: (val) => {
                        self.interfaces.panTiltSlider.maxJogSpeed.update(val)
                    },
                    increment: 1000
                })

                telnetParser.on('data', line => {
                    // console.log(">>" + line + "<<");
                    self.interfaces["rotaryPlate"].read(line)
                })
            })
        })

        telnetConnection.connect(telnetParams)

        // mqtt callback
        mqtt.addConnectCallback(() => {
            console.log('mqtt connected');
            console.log("execute mqtt connectCallbacks");
            this.gui = new Gui({
                name: "gui"
            })
            this.hudScreen = this.gui.addScreen({
                name: "HUD",
                enterCallback: () => {
                    console.log("HUD enterCallback");
                    controller.assignButtonCallbacks(this.defaultButtons)
                    this.resetMode()
                },
                leaveCallback: () => {
                    console.log("HUD leaveCallback");
                },
                draw: () => {
                    return this.updateLCD()
                }
            })
            this.settingsPage = this.gui.addMenue({
                name: "settings",
                enterCallback: () => {
                    console.log("settings enterCallback");
                    controller.assignButtonCallbacks(this.menueButtons)
                },
                leaveCallback: () => {
                    console.log("settings leaveCallback");
                }
            })
            this.settingsPage.addNumber({
                name: "Jog Slider",
                value: this.interfaces.panTiltSlider.maxJogSpeed.value,
                drawCallback: () => {
                    return this.interfaces.panTiltSlider.maxJogSpeed.value
                },
                callback: (val) => {
                    console.log("callback " + val);
                    this.interfaces.panTiltSlider.maxJogSpeed.update(val)
                },
                increment: 1000
            })
            this.functionsPage = this.gui.addMenue({
                name: "functions",
                enterCallback: () => {
                    console.log("functions enterCallback");
                    controller.assignButtonCallbacks(this.menueButtons)
                },
                leaveCallback: () => {
                    console.log("functions leaveCallback");
                }
            })
            this.functionsPage.addFunction({
                name: "Clear alarm slider",
                callback: () => {
                    this.killAlarmLock("panTiltSlider")
                }
            }).addFunction({
                name: "Clear alarm rotary plate",
                callback: () => {
                    this.killAlarmLock("rotaryPlate")
                }
            })

            this.refreshEditorString();
            this.gui.draw()

            for (const [name, interfaceInstance] of Object.entries(this.interfaces)) {
                interfaceInstance.statusQuery();
            }

            functionStore.addFunction(KEYFRAME_EDITOR_BASETOPIC + "/playAllKeyframes", (value) => {
                console.log("PLAY ALL KEYFRAMES");
                this.playAllKeyframes()
            })

            for (const [key, value] of Object.entries(this.interfaces)) {
                var interfaceInstance = value
                interfaceInstance.status.addCallback((value) => {
                    this.gui.draw()
                })
            };
        });

        // Setup keyframe file
        try {
            const data = fs.readFileSync(KEYFRAME_FILENAME, 'utf8')
            console.log("Keyframes file does exist.")
            console.log("Restoring keyframes...")
            try {
                var loadedKeyframes = JSON.parse(data);
                loadedKeyframes.forEach((newKeyframe) => {
                    this.keyframes.push(new Keyframe(newKeyframe))
                    this.keyframeCount++;
                    this.keyframePointer++;
                });
                console.log("done.");
                console.log(this.keyframes);
                // console.log(JSON.stringify(this.keyframes, null, 1));
            } catch (e) {
                console.error(e);
            }

        } catch (err) {
            console.log("Keyframes file does not exist.")
            console.log("Creating file...")

            this.saveKeyframes()
            console.log("done.");
        }

        // update all interfaces
        for (const [key, value] of Object.entries(this.interfaces)) {
            var interfaceInstance = value
            try {
                interfaceInstance.statusQuery()
            } catch (e) {}
        };
    } //end of constructor

    // Functions
    /**
     * [allInterfacesAreIdle]
     * check if all interfaces are in the state idle
     * @return {bool} true if all interfaces are idle
     */
    allInterfacesAreIdle() {
        var allInterfacesIdle = true
        for (const [name, interfaceInstance] of Object.entries(this.interfaces)) {
            if (interfaceInstance.status.value != "Idle") {
                allInterfacesIdle = false
            }
        }
        return allInterfacesIdle
    }

    /**
     * [toggleInclude]
     * toggles the include state of the specified axis of the specified interface
     * @param  {string}  interfaceName 	the name of the interface
     * @param  {string} axisName      	the name of the axis
     */
    toggleInclude(interfaceName, axisName) {
        this.interfaces[interfaceName].axes[axisName].toggleInclude.callFunction()
        this.gui.draw()
    }

    /**
     * [jog]
     * execute the jog function of the specified interface instance
     * @param  {string} interfaceInstance name of the interface instance to jog
     * @param  {object} coordinates object that contains the axis names and jogfactors
     * 								of the specified interface to jog to
     */
    jog(interfaceInstance, coordinates) {
        this.interfaces[interfaceInstance].jog(coordinates)
    }

    /**
     * [jogCancel]
     * execute the jogCancel function of the specified interface instance
     * @param  {string} interfaceInstance name of the interface instance to jogCancel
     */
    jogCancel(interfaceInstance) {
        this.interfaces[interfaceInstance].jogCancel()
    }

    /**
     * [softReset]
     * execute the softReset function of the specified interface instance
     * @param  {string} interfaceInstance name of the interface instance to softReset
     */
    softReset(interfaceInstance) {
        this.interfaces[interfaceInstance].softReset()
    }

    /**
     * [setZero]
     * execute the setZero function of the specified interface instance
     * @param  {string} interfaceInstance name of the interface instance to setZero
     */
    setZero(interfaceInstance) {
        this.interfaces[interfaceInstance].setZero()
    }

    /**
     * [killAlarmLock]
     * execute the killAlarmLock function of the specified interface instance
     * @param  {string} interfaceInstance name of the interface instance to killAlarmLock
     */
    killAlarmLock(interfaceInstance) {
        this.interfaces[interfaceInstance].killAlarmLock()
    }

    /**
     * [home]
     * execute the home function of the specified interface instance
     * @param  {string} interfaceInstance name of the interface instance to home
     */
    home(interfaceInstance) {
        this.interfaces[interfaceInstance].home()
    }

    /**
     * [updateLCD]
     * creates a drawbuffer for the gui
     * @return {[type]} [an array of strings]
     */
    updateLCD() {
        var drawbuffer = []
        var keyframeStatus = "";
        if (this.keyframes.length > 0) {
            if (this.keyframePointer > 0) {
                keyframeStatus += "<";
            } else {
                keyframeStatus += "[";
            }
            keyframeStatus += ("0" + this.keyframePointer).slice(-2);
            if (this.keyframePointer == this.keyframes.length - 1) {
                keyframeStatus += "]";
            } else {
                keyframeStatus += ">";
            }
        } else {
            keyframeStatus = "|KF|";
        }

        // .123456789.123456789
        try {
            drawbuffer = [
                this.interfaces["panTiltSlider"].axes["A"].toString() + ("      " + this.interfaces["panTiltSlider"].status.value).slice(-5) + ("     " + this.interfaces["panTiltSlider"].maxJogSpeed.value).slice(-7),
                this.interfaces["panTiltSlider"].axes["B"].toString() + ("      " + this.interfaces["rotaryPlate"].status.value).slice(-5) + ("     " + this.interfaces["rotaryPlate"].maxJogSpeed.value).slice(-7),
                this.interfaces["rotaryPlate"].axes["C"].toString() + "    " + (this.mode + "    ").substring(0, 4) + keyframeStatus,
                this.interfaces["panTiltSlider"].axes["X"].toString() + "     " + this.editorString
            ]
        } catch (e) {}

        return drawbuffer
    }

    /**
     * [playAllKeyframes]
     * moves all interfaces to the position of the first keyframe,
     * waits for all interfaces to arrive,
     * waits for a second
     * and then plays all keyframes
     */
    playAllKeyframes() {
        console.log("Play all keyframes");
        if (this.keyframes.length < 1) {
            return;
        }

        console.log("Move axis to start");
        // move all interfaces to the position of the first keyframe
        for (const [name, interfaceVector] of Object.entries(this.keyframes[0].interfaces)) {
            this.interfaces[name].play()
            this.interfaces[name].addToCommandBuffer('G94\r');
            this.interfaces[name].addToCommandBuffer('G90\r');
            this.interfaces[name].addToCommandBuffer(this.interfaces[name].createKeyframeCode(interfaceVector.axes, 0))
        }

        console.log("Wait for all axes to finish");

        // wait until all axes are idle
        this.allInterfacesIdleCallback = () => {
            console.log("allInterfacesIdleCallback");
            var i = 0
            for (const [name, keyframe] of Object.entries(this.keyframes)) {
                for (const [name, interfaceVector] of Object.entries(keyframe.interfaces)) {
                    if (i == 0) {
                        this.interfaces[name].pause()
                        this.interfaces[name].addToCommandBuffer('G94\r');
                        this.interfaces[name].addToCommandBuffer('G90\r');
                        this.interfaces[name].addToCommandBuffer("G4P1\r");
                    } else {
                        this.interfaces[name].addToCommandBuffer(interfaceVector.code)
                    }
                };
                i++
            };

            for (const [name, interfaceInstance] of Object.entries(this.interfaces)) {
                interfaceInstance.play()
            }
            this.allInterfacesIdleCallback = undefined;
        }

        // if all interfaces are already at start the status won't change
        // must happen after this function is left because we must let grbl do its thing first
        setTimeout(() => {
            console.log("timeout reached: " + this.allInterfacesAreIdle());
            if (this.allInterfacesAreIdle()) {
                if (allInterfacesIdleCallback != undefined) {
                    this.allInterfacesIdleCallback()
                }
            }
        }, 200)
    }

    /**
     * [setModeAdd]
     * sets the mode of the keyframeEditor to add
     */
    setModeAdd() {
        this.mode = this.modes.add;
        this.gui.draw()
    }

    /**
     * [resetMode]
     * sets the mode of the keyframeEditor to empty
     */
    resetMode() {
        this.mode = this.modes.empty;
        this.gui.draw()
    }

    /**
     * [getKeyframeFromTime]
     *
     * @return {[type]} [description]
     */
    getKeyframeFromTime() {
        var tempKeyframe = new Keyframe({
            interfaces: {},
            t: 0
        })

        if (this.keyframes[this.keyframePointer] != undefined &&
            this.keyframes[this.keyframePointer + 1] != undefined) {
            // if one axis is not included calculate its value based on the
            // position in time between this and the next keyframe

            for (const [name, interfaceInstance] of Object.entries(this.interfaces)) {
                for (const [axisName, axis] of Object.entries(interfaceInstance.axes)) {
                    if (!axis.isIncluded()) {
                        // find the same axis in the previous keyframe
                        var valueA = undefined //previous keyframe
                        var timeA = undefined //previous keyframe
                        for (var i = this.keyframePointer; i > -1; i--) {
                            if (this.keyframes[i].interfaces[interfaceInstance.name].axes[axis.axisName].include) {
                                valueA = this.keyframes[i].interfaces[interfaceInstance.name].axes[axis.axisName].value
                                timeA = this.keyframes[i].t
                            }
                        }

                        var valueC = undefined //previous keyframe
                        var timeC = undefined //previous keyframe
                        for (var i = this.keyframePointer + 1; i < this.keyframes.length; i++) {
                            if (this.keyframes[i].interfaces[interfaceInstance.name].axes[axis.axisName].include) {
                                valueC = this.keyframes[i].interfaces[interfaceInstance.name].axes[axis.axisName].value
                                timeC = this.keyframes[i].t
                            }
                        }

                        // if both keyframes were found calculate the value of the coordinate
                        // based on the position in time between the two keyframes
                        if (valueA != undefined && timeA != undefined && valueC != undefined && timeC != undefined) {
                            if (tempKeyframe.interfaces[interfaceInstance.name] == undefined) {
                                tempKeyframe.interfaces[interfaceInstance.name] = {
                                    name: interfaceInstance.name,
                                    axes: {}
                                }
                            }

                            // fill in temp keyframe
                            // copy axis from existing keyframe
                            tempKeyframe.interfaces[interfaceInstance.name].axes[axis.axisName] = {}
                            Object.assign(tempKeyframe.interfaces[interfaceInstance.name].axes[axis.axisName],
                                this.keyframes[0].interfaces[interfaceInstance.name].axes[axis.axisName])

                            // replace the value with the calculated value
                            tempKeyframe.interfaces[interfaceInstance.name].axes[axis.axisName].value =
                                (valueA + ((valueC - valueA) * ((this.getTimeValue() - timeA) / (timeC - timeA))))
                        } else {
                            console.trace("PARTNER KEYFRAME NOT FOUND");
                        }
                    }
                }
            }
        }
        return tempKeyframe
    }

    /**
     * [moveToKeyframe]
     * moves all interfaces to the positions definde in the passed keyframe
     * @param  {[type]} keyframe [description]
     * @return {[type]}          [description]
     */
    moveToKeyframe(keyframe) {
        for (const [name, interfaceVector] of Object.entries(keyframe.interfaces)) {
            this.interfaces[name].play()
            this.interfaces[name].addToCommandBuffer('G94\r');
            this.interfaces[name].addToCommandBuffer('G90\r');
            this.interfaces[name].addToCommandBuffer(this.interfaces[name].createKeyframeCode(interfaceVector.axes, keyframe.t))
        }
    }

    /**
     * [playKeyframeAtPosition]
     * moves all interfaces to the keyframe before the keyframe at the keyframePointer position,
     * then plays the keyframe at the keyframePointer position
     * @return {[type]} [description]
     */
    playKeyframeAtPosition() {
        console.log("Play keyframe at position");
        if (this.keyframes.length < 1) {
            return;
        }

        if (this.keyframes[this.keyframePointer - 1] == undefined) {
            return;
        }
        console.log("Move axis to start");
        // move all interfaces to the position of the first keyframe
        for (const [name, interfaceVector] of Object.entries(this.keyframes[this.keyframePointer - 1].interfaces)) {
            this.interfaces[name].play()
            this.interfaces[name].addToCommandBuffer('G94\r');
            this.interfaces[name].addToCommandBuffer('G90\r');
            this.interfaces[name].addToCommandBuffer(this.interfaces[name].createKeyframeCode(interfaceVector.axes, 0))
        }

        console.log("Wait for all axes to finish");

        // wait until all axes are idle
        this.allInterfacesIdleCallback = () => {
            console.log("allInterfacesIdleCallback");
            for (const [name, interfaceVector] of Object.entries(this.keyframes[this.keyframePointer].interfaces)) {
                this.interfaces[name].pause()
                this.interfaces[name].addToCommandBuffer('G94\r');
                this.interfaces[name].addToCommandBuffer('G90\r');
                this.interfaces[name].addToCommandBuffer("G4P1\r");
                this.interfaces[name].addToCommandBuffer(interfaceVector.code)
            };

            for (const [name, interfaceInstance] of Object.entries(this.interfaces)) {
                interfaceInstance.play()
            }
            this.allInterfacesIdleCallback = undefined;
        }

        // if all interfaces are already at start the status won't change
        // must happen after this function is left because we must let grbl do its thing first
        setTimeout(() => {
            console.log("timeout reached: " + this.allInterfacesAreIdle());
            if (this.allInterfacesAreIdle()) {
                if (allInterfacesIdleCallback != undefined) {
                    this.allInterfacesIdleCallback()
                }
            }
        }, 200)
    }

    /**
     * [addKeyframe]
     * adds a keyframe based on the current position of all interfaces
     */
    addKeyframe() {
        console.log("addKeyframe");

        // set t = 0 if first keyframe
        var t = this.keyframes.length > 0 ? this.getTimeValue() : 0;
        this.keyframePointer++;
        console.log("this.keyframePointer: " + this.keyframePointer);

        var interfaces = {}

        for (const [key, value] of Object.entries(this.interfaces)) {
            var interfaceInstance = value
            interfaces[interfaceInstance.name] = interfaceInstance.getAxesVector()
        }
        this.keyframes.splice(this.keyframePointer, 0, new Keyframe({
            interfaces: interfaces,
            t: t
        }))
        this.keyframeCount++;

        this.sortKeyframes()
        this.updateAllGcodes()
        console.log(this.keyframes);
        this.saveKeyframes()

        this.includeX = true;
        this.includeY = true;
        this.includeA = true;
        this.includeB = true;
        this.gui.draw()
    }

    /**
     * [deleteKeyframe]
     * deletes the keyframe at the current position of the keyframePointer
     * or the provided position.
     * corrects the keyframePointer if it is out of bounds.
     * @param  {int} pos (optional) the position to delete
     * @return {[type]}     [description]
     */
    deleteKeyframe(pos) {
        if (this.keyframes.length < 1 | pos > this.keyframes.length - 1) {
            return;
        }
        pos != undefined ? this.keyframes.splice(pos, 1) : this.keyframes.pop();
        if (this.keyframePointer > this.keyframes.length - 1) {
            this.keyframePointer = this.keyframes.length - 1
        }
        console.log(this.keyframes);
        this.saveKeyframes()
        this.gui.draw()
    }

    /**
     * [saveKeyframes]
     * saves the keyframes to a file at KEYFRAME_FILENAME
     * @return {[type]} [description]
     */
    saveKeyframes() {
        fs.writeFile(KEYFRAME_FILENAME, JSON.stringify(this.keyframes), err => {
            if (err) {
                console.error("Problem writing Keyframes.")
                console.error(err)
                return
            }
        })
    }

    /**
     * [incKeyframePointer]
     * increments the keyframe pointer by 1
     * keeps the pointer within the bounds of the keyframes array
     * @return {[type]} [description]
     */
    incKeyframePointer() {
        if (this.keyframePointer < this.keyframes.length - 1) {
            this.keyframePointer++;
            this.setValue(this.keyframes[this.keyframePointer].t);
        }
        console.log(this.keyframePointer);
        this.gui.draw()
    }

    /**
     * [decKeyframePointer]
     * increments the keyframe pointer by 1
     * keeps the pointer within the bounds of the keyframes array
     * @return {[type]} [description]
     */
    decKeyframePointer() {
        if (this.keyframePointer > 0) {
            this.keyframePointer--;
            this.setValue(this.keyframes[this.keyframePointer].t);
        }
        console.log(this.keyframePointer);
        this.gui.draw()
    }

    /**
     * [getTimeValue]
     * returns the current time value of the keyframe editor in tenths of seconds
     * @return {int} current time value of the editor in tenths of seconds
     */
    getTimeValue() {
        return this.tenthSeconds +
            (10 * this.seconds) +
            (600 * this.minutes)
    }

    /**
     * [setValue]
     * sets the time value of the keyframe editor.
     * provide time value in tenths of seconds.
     * time values of the keyframe editor are calculated accordingly
     * @param {int} value time value in tenths of seconds
     */
    setValue(value) {
        this.tenthSeconds = value % 10
        this.seconds = Math.floor(value / 10)
        this.minues = Math.floor(value / 600)
        this.refreshEditorString();
        this.gui.draw()
    }

    /**
     * [incTimeEditPosition]
     * increments the position of the time value editor
     * 0 tenths of seconds
     * 1 seconds
     * 2 minutes
     * @return none
     */
    incTimeEditPosition() {
        this.timeEditPosition++;
        if (this.timeEditPosition > 2) {
            this.timeEditPosition = 2;
        }
        this.refreshEditorString()
    }

    /**
     * [decTimeEditPosition]
     * decrements the position of the time value editor
     * 0 tenths of seconds
     * 1 seconds
     * 2 minutes
     * @return none
     */
    decTimeEditPosition() {
        this.timeEditPosition--;
        if (this.timeEditPosition < 0) {
            this.timeEditPosition = 0;
        }
        this.refreshEditorString()
    }

    /**
     * [incTime]
     * increments the time value at the current time edit position
     * overflow is carried over to the next digit
     * @return {[type]} [description]
     */
    incTime() {
        switch (this.timeEditPosition) {
            case 0:
                this.incTenthSeconds();
                break;
            case 1:
                this.incSeconds();
                break;
            case 2:
                this.incMinutes();
                break;
            default:
                console.error("this.timeEditPosition");
                console.error(this.timeEditPosition);
                break;
        }
        this.refreshEditorString();
        this.moveToKeyframe(this.getKeyframeFromTime())
    }

    /**
     * [decTime]
     * decrements the time value at the current time edit position
     * overflow is carried over to the next digit
     * @return {[type]} [description]
     */
    decTime() {
        switch (this.timeEditPosition) {
            case 0:
                this.decTenthSeconds();
                break;
            case 1:
                this.decSeconds();
                break;
            case 2:
                this.decMinutes();
                break;
            default:
                console.error("this.timeEditPosition");
                console.error(this.timeEditPosition);
                break;
        }
        this.refreshEditorString();
        this.moveToKeyframe(this.getKeyframeFromTime())
    }

    /**
     * [incTenthSeconds]
     * increments the tenths of seconds value of the editor
     * overflow is carried over to the next digit
     * @return {[type]} [description]
     */
    incTenthSeconds() {
        if (this.minutes == this.maxMinutes) {
            return;
        }
        this.tenthSeconds++;
        if (this.tenthSeconds > 9) {
            this.tenthSeconds = 0;
            this.incSeconds();
        }
        this.gui.draw()
    }

    /**
     * [decTenthSeconds]
     * decrements the tenths of seconds value of the editor
     * overflow is carried over to the next digit
     * @return {[type]} [description]
     */
    decTenthSeconds() {
        if (this.tenthSeconds == 0 &&
            this.seconds == 0 &&
            this.minutes == 0
        ) {
            return
        }
        this.tenthSeconds--;
        if (this.tenthSeconds < 0) {
            this.tenthSeconds = 9;
            this.decSeconds();
        }
        this.gui.draw()
    }

    /**
     * [incSeconds]
     * increments the seconds value of the editor
     * overflow is carried over to the next digit
     * @return {[type]} [description]
     */
    incSeconds() {
        if (this.minutes == this.maxMinutes) {
            return;
        }
        this.seconds++;
        if (this.seconds > 59) {
            this.seconds = 0;
            this.incMinutes();
        }
        this.gui.draw()
    }

    /**
     * [decSeconds]
     * decrements the seconds value of the editor
     * overflow is carried over to the next digit
     * @return {[type]} [description]
     */
    decSeconds() {
        if (this.tenthSeconds == 0 &&
            this.seconds == 0 &&
            this.minutes == 0
        ) {
            return
        }
        this.seconds--;
        if (this.seconds < 0) {
            this.seconds = 59;
            this.decMinutes();
        }
        this.gui.draw()
    }

    /**
     * [incMinutes]
     * increments the minutes value of the editor
     * overflow is carried over to the next digit
     * enshures maxMinutes limit is met
     * @return {[type]} [description]
     */
    incMinutes() {
        if (this.minutes == this.maxMinutes) {
            return;
        }
        this.minutes++;
        if (this.minutes > this.maxMinutes) {
            this.minutes = this.maxMinutes;
        }
        this.gui.draw()
    }

    /**
     * [decMinutes]
     * decrements the minutes value of the editor
     * overflow is carried over to the next digit
     * @return {[type]} [description]
     */
    decMinutes() {
        if (this.tenthSeconds == 0 &&
            this.seconds == 0 &&
            this.minutes == 0
        ) {
            return
        }
        this.minutes--;
        if (this.minutes < 0) {
            this.minutes = 0;
        }
        this.gui.draw()
    }

    /**
     * [refreshEditorString]
     * creates a string representation of the time editor in editorString
     * @return {[type]} [description]
     */
    refreshEditorString() {
        this.editorString = ("0" + this.minutes).slice(-2) + this.timeEditPositionStrings[this.timeEditPosition] + ("0" + this.seconds).slice(-2) + "." + this.tenthSeconds
        this.gui.draw()
    }

    /**
     * [compareKeyframes]
     * compare function to use the sort function
     * @param  {[type]} a [keyframe a]
     * @param  {[type]} b [keyframe b]
     * @return {[type]}   [0 equal, 1 a>b, -1 a<b]
     */
    compareKeyframes(a, b) {
        if (a.t < b.t) {
            return -1
        }
        if (a.t > b.t) {
            return 1
        }
        return 0;
    }

    /**
     * [sortKeyframes]
     * sort the keyframes based on their time value
     */
    sortKeyframes() {
        this.keyframes.sort(this.compareKeyframes)
        console.log("sortKeyframes");
        console.log(this.keyframes);
    }

    /**
     * [updateAllGcodes]
     * updates the gcode of all keyframes because there may have been
     * new keyframes been insertet between
     */
    updateAllGcodes() {
        this.keyframes[0].createGcode(0);
        for (var i = 1; i < this.keyframes.length; i++) {
            this.keyframes[i].createGcode(this.keyframes[i].t - this.keyframes[i - 1].t)
        }
    }

    /**
     * [debug]
     * print the keyframeEditor Object
     */
    debug() {
        console.error(this);
    }
}

/**
 * keyframe class
 * calculates the F value upon creation
 * generates the gcode command to execute the keyframe
 */
class Keyframe {
    // constructor(x, y, z, a, t, includeA, includeB, includeX, includeY, f, gcode, keyframeIndex) {
    constructor(options) {
        this.interfaces = options.interfaces
        this.t = options.t
    }

    /**
     * duration in 0.1s steps
     */
    createGcode(duration) {
        if (duration == undefined) {
            console.error("duration must be defined");
            return;
        }
        for (const [name, interfaceVector] of Object.entries(this.interfaces)) {
            interfaceVector.code = keyframeEditor.interfaces[interfaceVector.name].createKeyframeCode(interfaceVector.axes, duration)
            console.log(interfaceVector.code);
        };
    }

}

var keyframeEditor = new KeyframeEditor()
module.exports = keyframeEditor;
