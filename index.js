var controller = new require("./controller.js").controller;
var editor = require("./keyframeEditor.js");
var mqtt = require("./mqttHandler.js").mqttHandler

mqtt.addConnectCallback(() => {
    controller.assignButtonCallbacks(defaultButtons);
    controller.assignAxisCallbacks(defaultAxisCallbacks);
})

var addKeyframeButtons = [{
    name: "X",
    trigger: "fall",
    callback: (value) => {
        editor.toggleInclude("panTiltSlider", "A")
    }
}, {
    name: "Y",
    trigger: "fall",
    callback: (value) => {
        editor.toggleInclude("panTiltSlider", "B")
    }
}, {
    name: "A",
    trigger: "fall",
    callback: (value) => {
        editor.toggleInclude("panTiltSlider", "X")
    }
}, {
    name: "B",
    trigger: "fall",
    callback: (value) => {
        editor.toggleInclude("rotaryPlate", "C")
    }
}, {
    name: "PLUS",
    trigger: "fall",
    callback: (value) => {
        editor.addKeyframe();
        controller.assignButtonCallbacks(defaultButtons);
        editor.resetMode();
    }
}, {
    name: "MINUS",
    trigger: "fall",
    callback: (value) => {
        controller.assignButtonCallbacks(defaultButtons);
        editor.resetMode();
    }
}]

var menueButtons = [{
    name: "UP",
    trigger: "fall",
    callback: (value) => {
        editor.gui.up();
    }
}, {
    name: "DOWN",
    trigger: "fall",
    callback: (value) => {
        editor.gui.down();
    }
}, {
    name: "LEFT",
    trigger: "fall",
    callback: (value) => {
        editor.gui.escape();
    }
}, {
    name: "RIGHT",
    trigger: "fall",
    callback: (value) => {
        editor.gui.action();
    }
}, {
    name: "R1",
    trigger: "fall",
    callback: (value) => {
        editor.gui.nextPage()
    }
}, {
    name: "L1",
    trigger: "fall",
    callback: (value) => {
        editor.gui.previousPage()
    }
}]

var defaultButtons = [
    //Keyframes Buttons
    {
        name: "R3",
        trigger: "fall",
        callback: (value) => {
            editor.debug();
        }
    },
    {
        name: "NEXT",
        trigger: "fall",
        callback: (value) => {
            editor.incKeyframePointer();
        }
    }, {
        name: "PREV",
        trigger: "fall",
        callback: (value) => {
            editor.decKeyframePointer();
        }
    }, {
        name: "PLAY",
        trigger: "fall",
        callback: (value) => {
            editor.playKeyframeAtPosition();
        }
    }, {
        name: "PLUS",
        trigger: "fall",
        callback: (value) => {
            controller.assignButtonCallbacks(addKeyframeButtons);
            editor.setModeAdd()
        }
    }, {
        name: "MINUS",
        trigger: "fall",
        callback: (value) => {
            editor.deleteKeyframe()
        }
    }, {
        name: "START",
        trigger: "fall",
        callback: (value) => {
            editor.playAllKeyframes();
        }
    }, {
        name: "UP",
        trigger: "fall",
        callback: (value) => {
            editor.incTime();
        }
    }, {
        name: "DOWN",
        trigger: "fall",
        callback: (value) => {
            editor.decTime();
        }
    }, {
        name: "LEFT",
        trigger: "fall",
        callback: (value) => {
            editor.incTimeEditPosition();
        }
    }, {
        name: "RIGHT",
        trigger: "fall",
        callback: (value) => {
            editor.decTimeEditPosition();
        }

        //Jog Buttons
    }, {
        name: "B",
        trigger: "fall",
        callback: (value) => {
            // right
            editor.jog("rotaryPlate", {
                C: 0.02
            })
        }
    }, {
        name: "X",
        trigger: "fall",
        callback: (value) => {
            editor.jog("rotaryPlate", {
                C: -0.02
            })
        }
    }, {
        name: "R2",
        trigger: "fall",
        callback: (value) => {
            editor.jog("panTiltSlider", {
                X: 0.6
            })
        }
    }, {
        name: "L2",
        trigger: "fall",
        callback: (value) => {
            editor.jog("panTiltSlider", {
                X: -0.6
            })
        }
    }, {
        name: "R1",
        trigger: "fall",
        callback: (value) => {
            editor.gui.nextPage()
        }
    }, {
        name: "L1",
        trigger: "fall",
        callback: (value) => {
            editor.gui.previousPage()
        }
    },
    //Homing
    {
        name: "HOME",
        trigger: "fall",
        callback: (value) => {
            // editor.setZero()
            editor.home("panTiltSlider");
            editor.setZero("rotaryPlate")
        }
    },
    //Jog Cancel
    {
        name: "B",
        trigger: "rise",
        callback: (value) => {
            // right
            editor.jogCancel("rotaryPlate");
        }
    }, {
        name: "X",
        trigger: "rise",
        callback: (value) => {
            // left
            editor.jogCancel("rotaryPlate");
        }
    }, {
        name: "R2",
        trigger: "rise",
        callback: (value) => {
            editor.jogCancel("panTiltSlider");
        }
    }, {
        name: "L2",
        trigger: "rise",
        callback: (value) => {
            editor.jogCancel("panTiltSlider");
        }
    }
]


// controller.assignAxisCallbacks([
// 	{name: "RX", trigger: "event", callback: (value)=>{console.log("RX value: " + value);}},
// 	{name: "RY", trigger: "event", callback: (value)=>{console.log("RY value: " + value);}},
// 	{name: "LX", trigger: "event", callback: (value)=>{console.log("LX value: " + value);}},
// 	{name: "LY", trigger: "event", callback: (value)=>{console.log("LY value: " + value);}}
// ])


var defaultAxisCallbacks = [{
    name: "RX",
    trigger: "event",
    callback: (value) => {
        // console.log(controller.axisPosition);
        editor.jog("rotaryPlate", {
            C: value
        });
    }
}, {
    name: "LX",
    trigger: "event",
    callback: (value) => {
        // console.log(controller.axisPosition);
        editor.jog("panTiltSlider", {
            A: value
        });
    }
}, {
    name: "LY",
    trigger: "event",
    callback: (value) => {
        // console.log(controller.axisPosition);
        editor.jog("panTiltSlider", {
            B: value
        });
    }
}, {
    name: "R",
    trigger: "center",
    callback: () => { //0x85 = 133 => jog cancel
        // console.log("CENTER R");
        // editor.jogCancel();
    }
}, {
    name: "LX",
    trigger: "center",
    callback: () => { //0x85 = 133 => jog cancel
        // console.log("CENTER LX");
        // editor.jogCancel();
    }
}]

editor.defaultButtons = defaultButtons
editor.addKeyframeButtons = addKeyframeButtons
editor.menueButtons = menueButtons
