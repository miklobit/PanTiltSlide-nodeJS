module.exports = {
    CONTROLLER_BASETOPIC: "controller",
    KEYFRAME_EDITOR_BASETOPIC: "keyframeEditor",
    AXIS_BASETOPIC: "axis",
    FUNCTIONS_BASETOPIC: "functions",
    GLOBAL_BASETOPIC: "motionControlRig",
    SETTINGS_BASETOPIC: "settings",
    STATUS_BASETOPIC: "status",
	INTERFACE_BASETOPIC: "interface",
}
