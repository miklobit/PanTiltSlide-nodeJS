var mqtt = require("./mqttHandler.js").mqttHandler

const STATUS_BASETOPIC = require("./mqttBasetopics.js").STATUS_BASETOPIC

// publishes the set value over mqtt
// the value can not be written over mqtt
// also calls all local callbacks
class StatusStore {
    constructor() {
        this._statuses = [];
    }

    addStatus(name, value, callback, topic) {
        if (this._statuses[name] == undefined) {
            this._statuses[name] = new Status(name, value, callback, topic)
        } else {
            console.error("Status name is taken: " + name);
            return
        }

        if (value != undefined) {
            this.update(name, value)
        }
        return this._statuses[name]
    }

    addCallback(name, callback) {
        try {
            this._statuses[name].addCallback(value)
        } catch (e) {
            // console.error(e);
        }
    }

    update(name, value) {
        try {
            // console.log(this);
            this._statuses[name].update(value)
        } catch (e) {
            // console.error(e);
        }
    }
}

class Status {
    constructor(name, value, callback, topic) {
        this.name = name
        this.value = value
		this.callbacks = []

        if (topic != undefined) {
            this.basetopic = topic
        } else {
            this.basetopic = mqtt.BASETOPIC + "/" + STATUS_BASETOPIC + "/" + this.name
        }

        if (callback != undefined) {
            this.addCallback(callback)
        }
    }
    _executeCallbacks() {
        this.callbacks.forEach(callback => {
            callback(this.value)
        });
    }

    update(value) {
		if (value != this.value) {
			this.value = value
			// console.log("[" + this.name + "]" + "Set status: " + value);
			this._executeCallbacks();
			mqtt.publish(this.basetopic + "/R", this.value);
		}
    }

    addCallback(callback) {
        this.callbacks.push(callback)
    }
}

var statusStore = new StatusStore();
module.exports.statusStore = statusStore;
