var settingStore = require('./mqttSettings.js').settingStore
var functionStore = require('./mqttFunctions.js').functionStore
var statusStore = require('./mqttStatus.js').statusStore
var mqtt = require("./mqttHandler.js").mqttHandler
const fs = require('fs');

const Interface = require("./interface.js")

const grblErrorCodes = JSON.parse(fs.readFileSync('grblErrorCodes.json'));
const grblAlarmCodes = JSON.parse(fs.readFileSync('grblAlarmCodes.json'));

const MODE_BASIC = 0
const MODE_CLEAR_ALL_BUFFERS = 1
const MODE_AWAIT_OK = 2
const MODE_PAUSE = 3

const TRAVEL_TO_FIRST_KEYFRAME_SPEED = 5000

module.exports = class Grbl extends Interface {
    // writeFunction, name, rxBufferSpace, jogIncrementTime, statusInterval, sendbufferInterval, axes
    constructor(options) {
        super({
            name: options.name,
            maxJogSpeed: options.maxJogSpeed,
            statusCallback: options.statusCallback,
            pause: () => {
                console.log("[" + this.name + "]" + "Pause");
                this.mode = MODE_PAUSE
            },
            play: () => {
                console.log("[" + this.name + "]" + "Play");
                this.mode = MODE_BASIC
            },
            addToCommandBuffer: (command) => {
                if (Array.isArray(command)) {
                    this.commandBuffer = this.commandBuffer.concat(command)
                } else {
                    this.commandBuffer.push(command)
                }
            },
            createKeyframeCode: (axes, duration) => {
                if (duration == undefined) {
                    return;
                }
                var fNumber = 600 / duration;
                if (!isFinite(fNumber)) {
                    fNumber = TRAVEL_TO_FIRST_KEYFRAME_SPEED;
                } else {
                    console.error("[" + this.name + "]" + "f is not finite");
                    console.error("[" + this.name + "]" + duration);
                    console.error("[" + this.name + "]" + axes);
                }
                var command = "G93G1"
                for (const [name, axis] of Object.entries(axes)) {
                    command += axis.internalName + axis.value.toFixed(2)
                }
                command += "F"
                command += fNumber
                command += '\r'
                return command
            },
            moveSingleAxisFunction: (axis) => {
                console.log("[" + this.name + "]" + "move single axis: " + axis.basetopic)
                this.addToCommandBuffer([
                    'G94\r',
                    'G90\r',
                    "G0" + axis.internalName + axis.getValue() + '\r'
                ])
            }
        })

        var self = this;
        this.writeFunction = options.writeFunction


        // buffer for answers from grbl
        this.grblReceiveBuffer = ""

        // modes and callbacks to control flow for more restricted operations
        // e.g. homing
        // the callbacks bear the responsibility to clear themself
        // and eventually return to MODE_BASIC
        this.mode = MODE_BASIC;
        this.clearAllBuffersCallback = undefined;
        this.awaitOkCallback = undefined;

        // the interval with which to call the status function of grbl
        // the status call updates the local coordinates
        this.statusInterval = options.statusInterval;

        this.sendbufferInterval = options.sendbufferInterval;

        // reference to the status update interval
        // will be fille once the seriall connection is established
        this.statusIntervalTimer = undefined;
        this.sendBufferIntervalTimer = undefined;
        // not used yet
        // will be used to count sent and parsed bytes to maximize throuput
        // to grbl
        this.rxBufferSpace = options.rxBufferSpace;
        // buffer array for gcode commands
        // the content of this buffer will be sent automatically to grbl
        this.commandBuffer = [];

        // time for a single jog command
        // used to calculate jog steps
        // shorter times for shorter delay => requires more responsive connection
        this.jogIncrementTime = options.jogIncrementTime;

        // set true to cancel all remaining jog commands
        this._jogCancel = false;

        // function to call after the position of grbl has been updated
        this.positionUpdateCallback = undefined;

        // the number of bytes in the grbl rx buffer
        this.bytesInRxBuffer = 0;

        // mimics the messages in the rx buffer
        this.sentBytes = [];

        self.statusIntervalTimer = setInterval(() => {
            if (this.mode == MODE_BASIC) {
                self.statusQuery();
            }
        }, self.statusInterval)

        self.sendBufferIntervalTimer = setInterval(() => {
            // console.log("[" + this.name + "]" + "this.mode: " + this.mode);
            if (this.mode == MODE_CLEAR_ALL_BUFFERS) {
                if (this.bytesInRxBuffer == 0) {
                    try {
                        this.clearAllBuffersCallback()
                    } catch (e) {
                        console.error("[" + this.name + "]" + "clearAllBuffersCallback not defined");
                        console.error("[" + this.name + "]" + e);
                    }
                }
            }
            if (this.mode == MODE_BASIC) {
                if (this.commandBuffer.length > 0) {
                    // if ( this.name == "rotaryPlate") {
                    // 	console.log(this.commandBuffer[0]);
                    // }
                    if (this._write(this.commandBuffer[0])) {
                        // console.log("[" + this.name + "]" + "SEND COMMAND");
                        // console.log("[" + this.name + "]" + "this.commandBuffer[0]: " + this.commandBuffer[0]);
                        this.commandBuffer.shift()
                    } else {
                        console.log("[" + this.name + "]" + "DID NOT SEND");
                        console.log("[" + this.name + "]" + "this.commandBuffer.length: " + this.commandBuffer.length);
                        console.log("[" + this.name + "]" + "this.commandBuffer: " + this.commandBuffer);
                        console.log("[" + this.name + "]" + "this.bytesInRxBuffer: " + this.bytesInRxBuffer);
                        console.log("[" + this.name + "]" + "this.commandBuffer[0].length: " + this.commandBuffer[0].length);
                    }
                } else {
                    // jog cancel
                    if (this._jogCancel) {
                        for (const [axisName, axis] of Object.entries(this.axes)) {
                            axis.setJogFactor(0)
                        }

                        // only cancel jog if all bytes are out of the receive buffer of grbl
                        if (this.bytesInRxBuffer == 0) {
                            this._jogCancel = false;

                            // send jog cancel
                            console.log("[" + this.name + "]" + "jog cancel");
                            this.writeFunction(Buffer.from([133]));
                        }
                    } else {
                        // if (this.jogX != 0 || this.jogY != 0 || this.jogZ != 0 || this.jogA != 0) {
                        if (Object.entries(this.axes).some(([name, axis]) => axis.getJogFactor() != 0)) {
                            // only jog when no unparsed commands are in buffer
                            // otherwise grbl will move after jog cancel
                            if (this.bytesInRxBuffer == 0) {
                                this._jogSpeed();
                            }
                        }
                    }
                }
            }
        }, self.sendBufferInterval)

        mqtt.addConnectCallback(() => {
            // axis can only be added once a mqtt connection is established
            // because axis uses mqttSettings etc

            options.axes.forEach((axis) => {
                this.addAxis(axis)
            });


            // console.log("***");
            // console.log(Object.entries(this.axes));
            // Object.entries(this.axes).forEach(([axisName, axis], i) => {
            // 	console.log(i);
            // 	console.log(axisName);
            // 	console.log(axis);
            // });
            // console.log("***");

            this.positionUpdateCallback = options.positionUpdateCallback

            this.status.addCallback((value) => {
                if (value == "Idle") {
                    this.grblError.update(grblErrorCodes[0])
                    this.grblAlarm.update(grblAlarmCodes[0])
                }
            })

            this.grblError = statusStore.addStatus(this.basetopic + "/error", grblErrorCodes[0], (newError) => {
                this.grblErrorMessage.update(newError.message)
                this.grblErrorDescription.update(newError.description)
            })

            this.grblErrorMessage = statusStore.addStatus(this.basetopic + "/error/message", grblErrorCodes[0].message, undefined, this.basetopic + "/error/message")
            this.grblErrorDescription = statusStore.addStatus(this.basetopic + "/error/description", grblErrorCodes[0].description, undefined, this.basetopic + "/error/description")

            this.grblAlarm = statusStore.addStatus(this.basetopic + "/alarm", grblAlarmCodes[0], (newAlarm) => {
                this.grblAlarmMessage.update(newAlarm.message)
                this.grblAlarmDescription.update(newAlarm.description)
            })

            this.grblAlarmMessage = statusStore.addStatus(this.basetopic + "/alarm/message", grblAlarmCodes[0].message, undefined, this.basetopic + "/alarm/message")
            this.grblAlarmDescription = statusStore.addStatus(this.basetopic + "/alarm/description", grblAlarmCodes[0].description, undefined, this.basetopic + "/alarm/description")

            functionStore.addFunction(this.basetopic + "/killAlarmLock", () => {
                // console.log("[" + this.name + "]" + "KILL ALARM LOCK");
                self.killAlarmLock();
            })

            functionStore.addFunction(this.basetopic + "/softReset", () => {
                // console.log("[" + this.name + "]" + "SOFT RESET");
                self.softReset();
            })

            functionStore.addFunction(this.basetopic + "/resume", () => {
                // console.log("[" + this.name + "]" + "RESUME");
                self.resume()
            })

            functionStore.addFunction(this.basetopic + "/statusReportQuery", () => {
                // console.log("[" + this.name + "]" + "STATUS REPORT QUERY");
                self.statusQuery();
            })

            functionStore.addFunction(this.basetopic + "/feedHold", () => {
                // console.log("[" + this.name + "]" + "FEED HOLD");
                self.feedHold()
            })

            functionStore.addFunction(this.basetopic + "/jogCancel", () => {
                // console.log("[" + this.name + "]" + "JOG CANCEL");
                self.jogCancel();
            })
        });
    }

    read(line) {
        this.grblReceiveBuffer = line.split("\r");

        for (var i = 0; i < this.grblReceiveBuffer.length; i++) {
            this.grblReceiveBuffer[i] = this.grblReceiveBuffer[i].replace("\n", "");

            // skip empty
            if (this.grblReceiveBuffer[i] == "") {
                continue;
            }

            // command has been proccessed => remove from sendbuffer
            if (this.grblReceiveBuffer[i] == "ok" || this.grblReceiveBuffer[i].startsWith("error:")) {
                // translate error code to text
                if (this.grblReceiveBuffer[i].startsWith("error:")) {
                    this.grblError.update(grblErrorCodes[parseInt(this.grblReceiveBuffer[i].replace("error:", ""))]);
                }

                // remove first entry from  sentByte FIFO List because it has been processed
                var first = this.sentBytes.shift()
                if (first != undefined) {
                    this.bytesInRxBuffer -= first;
                }

                if (this.mode == MODE_AWAIT_OK) {
                    if (this.grblReceiveBuffer[i] == "ok") {
                        try {
                            this.awaitOkCallback()
                        } catch (e) {
                            console.error("[" + this.name + "]" + "awaitOkCallback not defined");
                            console.error("[" + this.name + "]" + e);
                        }
                    }
                }
            } else if (this.grblReceiveBuffer[i].startsWith("ALARM:")) {
                // update alarm state
                this.grblAlarm.update(grblAlarmCodes[parseInt(this.grblReceiveBuffer[i].replace("ALARM:", ""))])
            }

            // parse statusQuery response
            if (this.grblReceiveBuffer[i].startsWith("<")) {

                var fields = this.grblReceiveBuffer[i].split("|")

                for (const field in fields) {
                    if (fields.hasOwnProperty(field)) {
                        const element = fields[field];

                        if (element.startsWith("<")) {
                            this.status.update(element.replace("<", ""));
                        }
                        if (element.startsWith("Bf")) {
                            this.rxBufferSpace = parseInt(element.replace("Bf:", "").split(",")[1])
                        }

                        if (element.startsWith("WPos") || element.startsWith("MPos")) {
                            if (element.indexOf(",") < 0) {
                                continue;
                            }

                            var dirty = false;

                            var coordinates = element.replace("WPos:", "").replace("MPos:", "").split(",");

                            if (coordinates == undefined) {
                                continue;
                            }
                            Object.entries(this.axes).forEach(([axisName, axis], i) => {
                                try {
                                    var value = parseFloat(coordinates[i]);
                                    if (value !== NaN) {
                                        if (value != axis.getValue()) {
                                            dirty = true;
                                            axis.setValue(value)
                                        }
                                    } else {
                                        console.error("parseFloat messed up");
                                        console.error(coordinates[i]);
                                    }
                                } catch (e) {
                                    console.error("Not enoug axes in grbl firmware defined");
                                    console.error(e);
                                }
                            });

                            // update if any value has changed
                            if (dirty) {
                                if (this.positionUpdateCallback != undefined) {
                                    // console.log(this.name);
                                    this.positionUpdateCallback();
                                }
                            }
                        }
                    }
                }
            }
        }
        this.grblReceiveBuffer = ""
    }


    _write(command) {
        if (this.bytesInRxBuffer + command.length <= this.rxBufferSpace) {
            this.bytesInRxBuffer += command.length;
            this.sentBytes.push(command.length)

            this.writeFunction(Buffer.from(command, 'utf-8'));
            return true;
        } else {
            console.error("[" + this.name + "]" + "CANT SEND");
            console.error("[" + this.name + "]" + "BUFFER WOULD OVERFLOW");
            console.error("[" + this.name + "]" + "command: " + command);
            console.error("[" + this.name + "]" + "command.length: " + command.length);
            console.error("[" + this.name + "]" + "this.rxBufferSpace: " + this.rxBufferSpace);
            console.error("[" + this.name + "]" + "this.bytesInRxBuffer: " + this.bytesInRxBuffer);
            return false;
        }
    }

    // realtime commands
    killAlarmLock() {
        this.writeFunction(Buffer.from("$X\r"))
        this.bytesInRxBuffer = 0;
        this.sentBytes = [];
    }
    resume() {
        this.writeFunction(Buffer.from('~'))
    }
    feedHold() {
        this.writeFunction(Buffer.from('!'))
    }
    statusQuery() {
        this.writeFunction(Buffer.from("?"))
    }
    softReset() {
        this.writeFunction(Buffer.from([24]));
        this.bytesInRxBuffer = 0;
        this.sentBytes = [];
    }

    // sets the jog factors
    // the sendBufferInterval handles the rest
    jog(factors) {
        // limit imput to minimum if >0
        for (const [key, value] of Object.entries(factors)) {
            var newValue = value
            try {
                var axis = this.axes[key]
                // implement deadzone
                if ((Math.abs(newValue) < axis.jogMin) && (newValue != 0)) {
                    if (newValue < 0) {
                        axis.setJogFactor(axis.jogMin * -1)
                    } else {
                        axis.setJogFactor(axis.jogMin)
                    }
                }
                axis.setJogFactor(newValue)
            } catch (e) {
                console.error("Axis not defined");
                console.error(e);
            }
        }

        if (Object.entries(this.axes).every(([axisName, axis]) => {
                axis.getJogFactor() == 0
            })) {
            this.jogCancel();
        }
    }

    /**
     * call repeatedly for joystic jogging
     */
    _jogSpeed() {

        var jogSpeed = 0

        Object.entries(this.axes).forEach(([axisName, axis], i) => {
            jogSpeed += Math.pow(axis.getJogFactor(), 2)
        });

        jogSpeed = Math.abs(Math.round(this.maxJogSpeed.value * (Math.sqrt(jogSpeed))))

        // temp value to calculate speed vectors
        var q = (this.maxJogSpeed.value / 60) * this.jogIncrementTime

        // header for all jog commands
        var command = '$J=G91'

        // dirty flag to move only when there is movement
        var noAxisIsNotZero = true
        Object.entries(this.axes).forEach(([axisName, axis], i) => {
            var s = axis.getJogFactor() * q
            if (s != 0) {
                noAxisIsNotZero = false;
                command += axis.internalName + s.toFixed(2)
            }
        })

        command += "F"
        command += jogSpeed
        command += '\r'

        // scip if all are 0
        if (noAxisIsNotZero) {
            return;
        }
        this.addToCommandBuffer(command);
    }

    jogCancel() {
        //real time command => no response
        // console.log("[" + this.name + "]" + "cancel");
        this._jogCancel = true;
    }

    setZero() {
        console.log("[" + this.name + "]" + "set zero");
        var command = "G10P1L20"
        Object.entries(this.axes).forEach(([axisName, axis], i) => {
            command += axis.internalName + "0"
        });
        command += "\r"

        this.addToCommandBuffer(command);
    }

    // homing blocks all other interaction with grbl
    home() {
        console.log("[" + this.name + "]" + "home");
        this.status.update("Home")

        // wait until all buffers are clear => all actions have been performed
        this.mode = MODE_CLEAR_ALL_BUFFERS
        this.clearAllBuffersCallback = () => {
            console.log("[" + this.name + "]" + "clearAllBuffersCallback");
            this.clearAllBuffersCallback = undefined

            // send the homing command and wait for the "ok" after it finishes
            this.writeFunction(Buffer.from("$h\r"))
            this.mode = MODE_AWAIT_OK
            this.awaitOkCallback = () => {
                // when the "ok" has been received homing was a success
                // now we can add any offsets and zero the workspace
                console.log("[" + this.name + "]" + "awaitOkCallback");
                this.mode = MODE_BASIC
                this.commandBuffer = [];
                this.addToCommandBuffer("G91G0Y-3.5\r")
                this.setZero()
            }
        }
    }

    writeGrbl(command) {
        this.addToCommandBuffer(command)
    }
}
