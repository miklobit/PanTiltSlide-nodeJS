var controller = new require("./controller.js").controller;
var mqtt = require("./mqttHandler.js").mqttHandler;
var Gui = require("./gui.js");
var gui = new Gui({name:"gui"})
const SerialPort = require('serialport')
const Readline = require('@serialport/parser-readline')
const port = new SerialPort('/dev/ttyS0', {
    baudRate: 115200
})
const parser = new Readline()
port.pipe(parser)
// read from serial
parser.on('data', line => {console.log("<< " + line)});

var stdinReadline = require('readline');
var rl = stdinReadline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});

rl.on('line', (line) => {
    console.log(">>" + line);
	port.write(Buffer.from(line + '\r', 'utf-8'), err => {
		if (err) console.log(err);
	});
})

gui.addMenue({name: "menue1"})
	.addFork({name:"fork1"})
		.addNumber({name: "entry10", value: "0", callback: (val)=>{console.log("***number10 " + val +" ***")}, increment: 100})
		.addNumber({name: "entry11", value: "1", callback: (val)=>{console.log("***number11 " + val +" ***")}})
		.addNumber({name: "entry12", value: "2", callback: (val)=>{console.log("***number12 " + val +" ***")}})
		.addFork({name:"fork11"})
			.addNumber({name: "entry110", value: "0", callback: (val)=>{console.log("***number110 " + val +" ***")}, increment: 100})
			.addNumber({name: "entry111", value: "1", callback: (val)=>{console.log("***number111 " + val +" ***")}})
			.addNumber({name: "entry112", value: "2", callback: (val)=>{console.log("***number112 " + val +" ***")}})
	.parent
		.addFunction({name: "function11", callback: ()=>{console.log("***function11***")} })
		.addFunction({name: "function12", callback: ()=>{console.log("***function12***")} })
		.addFunction({name: "function13", callback: ()=>{console.log("***function13***")} })
.parent
	.addFork({name:"fork2"})
		.addNumber({name: "entry20", value: "0", callback: (val)=>{console.log("***number20 " + val +" ***")}, increment: 100})
		.addNumber({name: "entry21", value: "1", callback: (val)=>{console.log("***number21 " + val +" ***")}})
		.addNumber({name: "entry22", value: "2", callback: (val)=>{console.log("***number22 " + val +" ***")}})
		.addFork({name:"fork22"})
			.addNumber({name: "entry220", value: "0", callback: (val)=>{console.log("***number220 " + val +" ***")}, increment: 100})
			.addNumber({name: "entry221", value: "1", callback: (val)=>{console.log("***number221 " + val +" ***")}})
			.addNumber({name: "entry222", value: "2", callback: (val)=>{console.log("***number222 " + val +" ***")}})
	.parent
		.addFunction({name: "function21", callback: ()=>{console.log("***function21***")} })
		.addFunction({name: "function22", callback: ()=>{console.log("***function22***")} })
		.addFunction({name: "function23", callback: ()=>{console.log("***function23***")} })
.parent
	.addFork({name:"fork3"})
		.addNumber({name: "entry30", value: "0", callback: (val)=>{console.log("***number30 " + val +" ***")}, increment: 100})
		.addNumber({name: "entry31", value: "1", callback: (val)=>{console.log("***number31 " + val +" ***")}})
		.addNumber({name: "entry32", value: "2", callback: (val)=>{console.log("***number32 " + val +" ***")}})
		.addFork({name:"fork33"})
			.addNumber({name: "entry330", value: "0", callback: (val)=>{console.log("***number330 " + val +" ***")}, increment: 100})
			.addNumber({name: "entry331", value: "1", callback: (val)=>{console.log("***number331 " + val +" ***")}})
			.addNumber({name: "entry332", value: "2", callback: (val)=>{console.log("***number332 " + val +" ***")}})
	.parent
		.addFunction({name: "function31", callback: ()=>{console.log("***function31***")} })
		.addFunction({name: "function32", callback: ()=>{console.log("***function32***")} })
		.addFunction({name: "function33", callback: ()=>{console.log("***function33***")} })
.parent
	.addNumber({name: "entry 10", value: "0", callback: (val)=>{console.log("***number10 " + val +" ***")}, increment: 100})
	.addNumber({name: "entry 11", value: "1", callback: (val)=>{console.log("***number11 " + val +" ***")}})
	.addNumber({name: "entry 12", value: "2", callback: (val)=>{console.log("***number12 " + val +" ***")}})
.print()

gui.addScreen({
	name:"screen1",
	up: ()=>{console.log("screen1 up");},
	down: ()=>{console.log("screen1 down");},
	action: ()=>{console.log("screen1 action");},
	escape: ()=>{console.log("screen1 escape");},
	enterCallback: ()=>{console.log("screen1 enterCallback");},
	leaveCallback: ()=>{console.log("screen1 leaveCallback");},
	draw: ()=>{
		console.log("draw");
		return ["screen1"]
	}
})

gui.addScreen({
	name:"screen2",
	up: ()=>{console.log("screen2 up");},
	down: ()=>{console.log("screen2 down");},
	action: ()=>{console.log("screen2 action");},
	escape: ()=>{console.log("screen2 escape");},
	enterCallback: ()=>{console.log("screen2 enterCallback");},
	leaveCallback: ()=>{console.log("screen2 leaveCallback");},
	draw: ()=>{
		console.log("draw");
		return ["screen2"]
	}
})

gui.addScreen({
	name:"screen3",
	up: ()=>{console.log("screen3 up");},
	down: ()=>{console.log("screen3 down");},
	action: ()=>{console.log("screen3 action");},
	escape: ()=>{console.log("screen3 escape");},
	enterCallback: ()=>{console.log("screen3 enterCallback");},
	leaveCallback: ()=>{console.log("screen3 leaveCallback");},
	draw: ()=>{
		console.log("draw");
		return ["screen3"]
	}
})

mqtt.addConnectCallback(()=>{
	gui.draw(true);
	// console.log(gui);

	controller.assignAxisCallbacks([
		{name: "RX", trigger: "event", callback: (value)=>{console.log("RX event: " + value);}},
		{name: "RY", trigger: "event", callback: (value)=>{console.log("RY event: " + value);}},
		{name: "LX", trigger: "event", callback: (value)=>{console.log("LX event: " + value);}},
		{name: "LY", trigger: "event", callback: (value)=>{console.log("LY event: " + value);}},

		{name: "RX", trigger: "center", callback: (value)=>{console.log("RX center: " + value);}},
		{name: "RY", trigger: "center", callback: (value)=>{console.log("RY center: " + value);}},
		{name: "LX", trigger: "center", callback: (value)=>{console.log("LX center: " + value);}},
		{name: "LY", trigger: "center", callback: (value)=>{console.log("LY center: " + value);}},

		{name: "R", trigger: "center", callback: (value)=>{console.log("R center: " + value);}},
		{name: "L", trigger: "center", callback: (value)=>{console.log("L center: " + value);}}
	])

	controller.assignButtonCallbacks([
	    { name: "UP", trigger: "rise", callback: 	() => { gui.up() } },
	    { name: "DOWN", trigger: "rise", callback: 	() => { gui.down() } },
	    { name: "LEFT", trigger: "rise", callback: 	() => { gui.escape() } },
	    { name: "RIGHT", trigger: "rise", callback: () => { gui.action() } },
		{ name: "R1", trigger: "fall", callback: 	() => { gui.nextPage() } },
	    { name: "L1", trigger: "fall", callback: 	() => { gui.previousPage() } }
	]);

})
