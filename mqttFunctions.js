var mqtt = require("./mqttHandler.js").mqttHandler

const FUNCTIONS_BASETOPIC = require("./mqttBasetopics.js").FUNCTIONS_BASETOPIC

// calls all local callbacks when a value is written over mqtt.
// the written value is passed to all local callbacks and mirrored over mqtt
// once all callbacks have been executed.
// a local trigger of the function has the same effect

class FunctionStore {
    constructor() {
        this._functions = []
    }

    addFunction(name, callback, topic) {
        if (this._functions[name] == undefined) {
            this._functions[name] = new Function(name, callback, topic)
			return this._functions[name]
        }
    }

    addCallback(name, callback) {
        try {
            this._functions[name].addCallback(callback)
        } catch (e) {
            // console.log(e);
        }
    }
}

class Function {
    constructor(name, callback, topic) {
        this.name = name
        this.callbacks = []

        if (topic != undefined) {
            this.basetopic = topic
        } else {
            this.basetopic = mqtt.BASETOPIC + "/" + FUNCTIONS_BASETOPIC + "/" + name
        }

        if (callback != undefined) {
            this.addCallback(callback)
        }

        mqtt.subscribe(this.basetopic + "/W", (value) => {
            this.callFunction(value)
        })
    }

    _executeCallbacks(value) {
        this.callbacks.forEach(callback => {
            callback(value)
        });
    }

    callFunction(value) {
		if (value == undefined) {
			value = ""
		}
        this._executeCallbacks(this.name, value);
        mqtt.publish(this.basetopic + "/R", value);
    }

    addCallback(callback) {
        this.callbacks.push(callback)
    }
}


var functionStore = new FunctionStore();
module.exports.functionStore = functionStore;
