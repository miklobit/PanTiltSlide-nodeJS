var async = require('async');
var noble = require('@abandonware/noble');

var peripheralIdOrAddress = process.argv[2].toLowerCase();

noble.on('stateChange', function(state) {
  if (state === 'poweredOn') {
    noble.startScanning();
  } else {
    noble.stopScanning();
  }
});

noble.on('discover', function(peripheral) {
  if (peripheral.id === peripheralIdOrAddress || peripheral.address === peripheralIdOrAddress) {
    noble.stopScanning();

    console.log('peripheral with ID ' + peripheral.id + ' found');
    var advertisement = peripheral.advertisement;

    var localName = advertisement.localName;
    var txPowerLevel = advertisement.txPowerLevel;
    var manufacturerData = advertisement.manufacturerData;
    var serviceData = advertisement.serviceData;
    var serviceUuids = advertisement.serviceUuids;

    if (localName) {
      console.log('  Local Name        = ' + localName);
    }

    if (txPowerLevel) {
      console.log('  TX Power Level    = ' + txPowerLevel);
    }

    if (manufacturerData) {
      console.log('  Manufacturer Data = ' + manufacturerData.toString('hex'));
    }

    if (serviceData) {
      console.log('  Service Data      = ' + JSON.stringify(serviceData, null, 2));
    }

    if (serviceUuids) {
      console.log('  Service UUIDs     = ' + serviceUuids);
    }

    console.log();

    explore(peripheral);
  }
});

function explore(peripheral) {
  console.log('services and characteristics:');

  peripheral.on('disconnect', function() {
    process.exit(0);
  });

  peripheral.connect(function(error) {
    peripheral.discoverServices([], function(error, services) {
      var serviceIndex = 0;

      async.whilst(
        function () {
          return (serviceIndex < services.length);
        },
        function(callback) {
          var service = services[serviceIndex];
          var serviceInfo = service.uuid;

          if (service.name) {
            serviceInfo += ' (' + service.name + ')';
          }
          console.log(serviceInfo);

          service.discoverCharacteristics([], function(error, characteristics) {
            var characteristicIndex = 0;

            async.whilst(
              function () {
                return (characteristicIndex < characteristics.length);
              },
              function(callback) {
                var characteristic = characteristics[characteristicIndex];
                var characteristicInfo = '  ' + characteristic.uuid;

                if (characteristic.name) {
                  characteristicInfo += ' (' + characteristic.name + ')';
                }

                async.series([
                  function(callback) {
                    characteristic.discoverDescriptors(function(error, descriptors) {
                      async.detect(
                        descriptors,
                        function(descriptor, callback) {
                          if (descriptor.uuid === '2901') {
                            return callback(descriptor);
                          } else {
                            return callback();
                          }
                        },
                        function(userDescriptionDescriptor){
                          if (userDescriptionDescriptor) {
                            userDescriptionDescriptor.readValue(function(error, data) {
                              if (data) {
                                characteristicInfo += ' (' + data.toString() + ')';
                              }
                              callback();
                            });
                          } else {
                            callback();
                          }
                        }
                      );
                    });
                  },
                  function(callback) {
                        characteristicInfo += '\n    properties  ' + characteristic.properties.join(', ');

                    if (characteristic.properties.indexOf('read') !== -1) {
                      characteristic.read(function(error, data) {
                        if (data) {
                          var string = data.toString('ascii');

                          characteristicInfo += '\n    value       ' + data.toString('hex') + ' | \'' + string + '\'';
                        }
                        callback();
                      });
                    } else {
                      callback();
                    }
                  },
                  function() {
                    console.log(characteristicInfo);
                    characteristicIndex++;
                    callback();
                  }
                ]);
              },
              function(error) {
                serviceIndex++;
                callback();
              }
            );
          });
        },
        function (err) {
          peripheral.disconnect();
        }
      );
    });
  });
}



//
// const noble = require('@abandonware/noble');
//
// // RGB Light
// // localName: YONGNUO LED
// // id: fb0c450af806
// // address: fb:0c:45:0a:f8:06
// // serviceUuids: f000aa6004514000b000000000000000
//
// // Light 1
// // localName: YONGNUO LED
// // id: edecfdb4ed2d
// // address: ed:ec:fd:b4:ed:2d
// // serviceUuids: f000aa6004514000b000000000000000
//
// // Light 2
// // localName: YONGNUO LED
// // id: efad992e9598
// // address: ef:ad:99:2e:95:98
// // serviceUuids: f000aa6004514000b000000000000000
//
// noble.on('stateChange', function(state) {
//     if (state === 'poweredOn') {
//         // noble.startScanning();
//         noble.startScanning(["f000aa6004514000b000000000000000"]);
//     } else {
//         noble.stopScanning();
//     }
// });
//
// noble.on('discover', function(peripheral) {
// 	// peripheral.connect((error)=>{
// 	// 	console.log(">>> peripheral connected\n")
// 	// 	peripheral.discoverAllServicesAndCharacteristics( (error, services, characteristics)=>{
// 	// 		console.log(">>>>>> service discovered")
// 	// 		console.log(services);
// 	// 		console.log(characteristics);
// 	// 	})
// 	// })
//     console.log("localName: " + peripheral.advertisement.localName);
//     console.log("id: " + peripheral.id);
//     console.log("address: " + peripheral.address);
//     console.log("addressType: " + peripheral.addressType);
//     console.log("connectable: " + peripheral.connectable);
//     console.log("rssi: " + peripheral.rssi);
//     console.log("serviceUuids: " + peripheral.advertisement.serviceUuids);
//     console.log("serviceData: " + peripheral.advertisement.serviceData);
//     console.log("manufacturerData: " + peripheral.advertisement.manufacturerData);
//     console.log("txPowerLevel: " + peripheral.advertisement.txPowerLevel);
// 	console.log(peripheral);
//     console.log("****************************************");
// });
