[{
    "id": "6a0d6ea1.57141",
    "type": "tab",
    "label": "Bt-LEDs",
    "disabled": false,
    "info": ""
}, {
    "id": "7820a8e.84a8458",
    "type": "tab",
    "label": "Settings",
    "disabled": false,
    "info": ""
}, {
    "id": "25436c92.733984",
    "type": "tab",
    "label": "Status",
    "disabled": false,
    "info": ""
}, {
    "id": "440cacce.57aac4",
    "type": "tab",
    "label": "Functions",
    "disabled": false,
    "info": ""
}, {
    "id": "6684ba10.301104",
    "type": "tab",
    "label": "Keyframe Editor",
    "disabled": false,
    "info": ""
}, {
    "id": "16dba669.90683a",
    "type": "tab",
    "label": "Controller",
    "disabled": false,
    "info": ""
}, {
    "id": "5b9ee607.66d238",
    "type": "ui_base",
    "theme": {
        "name": "theme-light",
        "lightTheme": {
            "default": "#0094CE",
            "baseColor": "#0094CE",
            "baseFont": "-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif",
            "edited": true,
            "reset": false
        },
        "darkTheme": {
            "default": "#097479",
            "baseColor": "#097479",
            "baseFont": "-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif",
            "edited": false
        },
        "customTheme": {
            "name": "Untitled Theme 1",
            "default": "#4B7930",
            "baseColor": "#4B7930",
            "baseFont": "-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif"
        },
        "themeState": {
            "base-color": {
                "default": "#0094CE",
                "value": "#0094CE",
                "edited": false
            },
            "page-titlebar-backgroundColor": {
                "value": "#0094CE",
                "edited": false
            },
            "page-backgroundColor": {
                "value": "#fafafa",
                "edited": false
            },
            "page-sidebar-backgroundColor": {
                "value": "#ffffff",
                "edited": false
            },
            "group-textColor": {
                "value": "#1bbfff",
                "edited": false
            },
            "group-borderColor": {
                "value": "#ffffff",
                "edited": false
            },
            "group-backgroundColor": {
                "value": "#ffffff",
                "edited": false
            },
            "widget-textColor": {
                "value": "#111111",
                "edited": false
            },
            "widget-backgroundColor": {
                "value": "#0094ce",
                "edited": false
            },
            "widget-borderColor": {
                "value": "#ffffff",
                "edited": false
            },
            "base-font": {
                "value": "-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif"
            }
        },
        "angularTheme": {
            "primary": "indigo",
            "accents": "blue",
            "warn": "red",
            "background": "grey"
        }
    },
    "site": {
        "name": "Node-RED Dashboard",
        "hideToolbar": "false",
        "allowSwipe": "false",
        "lockMenu": "false",
        "allowTempTheme": "true",
        "dateFormat": "DD/MM/YYYY",
        "sizes": {
            "sx": 48,
            "sy": 48,
            "gx": 6,
            "gy": 6,
            "cx": 6,
            "cy": 6,
            "px": 0,
            "py": 0
        }
    }
}, {
    "id": "b6aa58b1.404988",
    "type": "Generic BLE",
    "z": "",
    "localName": "YONGNUO LED",
    "address": "ef:ad:99:2e:95:98",
    "uuid": "efad992e9598",
    "muteNotifyEvents": false,
    "operationTimeout": "",
    "characteristics": [{
        "uuid": "2a00",
        "name": "Device Name",
        "type": "org.bluetooth.characteristic.gap.device_name",
        "notifiable": false,
        "readable": true,
        "writable": true,
        "writeWithoutResponse": false
    }, {
        "uuid": "2a01",
        "name": "Appearance",
        "type": "org.bluetooth.characteristic.gap.appearance",
        "notifiable": false,
        "readable": true,
        "writable": false,
        "writeWithoutResponse": false
    }, {
        "uuid": "2a04",
        "name": "Peripheral Preferred Connection Parameters",
        "type": "org.bluetooth.characteristic.gap.peripheral_preferred_connection_parameters",
        "notifiable": false,
        "readable": true,
        "writable": false,
        "writeWithoutResponse": false
    }, {
        "uuid": "2a05",
        "name": "Service Changed",
        "type": "org.bluetooth.characteristic.gatt.service_changed",
        "notifiable": false,
        "readable": false,
        "writable": false,
        "writeWithoutResponse": false
    }, {
        "uuid": "f000aa6304514000b000000000000000",
        "name": "<Unnamed>",
        "type": "(Custom Type)",
        "notifiable": true,
        "readable": false,
        "writable": false,
        "writeWithoutResponse": false
    }, {
        "uuid": "f000aa6104514000b000000000000000",
        "name": "<Unnamed>",
        "type": "(Custom Type)",
        "notifiable": false,
        "readable": false,
        "writable": true,
        "writeWithoutResponse": true
    }]
}, {
    "id": "dddf3e90.753f9",
    "type": "ui_group",
    "z": "",
    "name": "Bt-LEDs",
    "tab": "5ef94a6e.825984",
    "order": 3,
    "disp": true,
    "width": "6",
    "collapse": false
}, {
    "id": "b5d6e9bd.919d38",
    "type": "mqtt-broker",
    "z": "",
    "name": "",
    "broker": "localhost",
    "port": "1883",
    "clientid": "",
    "usetls": false,
    "compatmode": false,
    "keepalive": "60",
    "cleansession": true,
    "birthTopic": "",
    "birthQos": "0",
    "birthPayload": "",
    "closeTopic": "",
    "closeQos": "0",
    "closePayload": "",
    "willTopic": "",
    "willQos": "0",
    "willPayload": ""
}, {
    "id": "5ef94a6e.825984",
    "type": "ui_tab",
    "z": "",
    "name": "Bt-LEDs",
    "icon": "dashboard",
    "order": 2,
    "disabled": false,
    "hidden": false
}, {
    "id": "967711c3.27ebb",
    "type": "Generic BLE",
    "z": "",
    "localName": "YONGNUO LED",
    "address": "ef:ad:99:2e:95:98",
    "uuid": "efad992e9598",
    "muteNotifyEvents": false,
    "operationTimeout": "",
    "characteristics": [{
        "uuid": "2a00",
        "name": "Device Name",
        "type": "org.bluetooth.characteristic.gap.device_name",
        "notifiable": false,
        "readable": true,
        "writable": true,
        "writeWithoutResponse": false
    }, {
        "uuid": "2a01",
        "name": "Appearance",
        "type": "org.bluetooth.characteristic.gap.appearance",
        "notifiable": false,
        "readable": true,
        "writable": false,
        "writeWithoutResponse": false
    }, {
        "uuid": "2a04",
        "name": "Peripheral Preferred Connection Parameters",
        "type": "org.bluetooth.characteristic.gap.peripheral_preferred_connection_parameters",
        "notifiable": false,
        "readable": true,
        "writable": false,
        "writeWithoutResponse": false
    }, {
        "uuid": "2a05",
        "name": "Service Changed",
        "type": "org.bluetooth.characteristic.gatt.service_changed",
        "notifiable": false,
        "readable": false,
        "writable": false,
        "writeWithoutResponse": false
    }, {
        "uuid": "f000aa6304514000b000000000000000",
        "name": "<Unnamed>",
        "type": "(Custom Type)",
        "notifiable": true,
        "readable": false,
        "writable": false,
        "writeWithoutResponse": false
    }, {
        "uuid": "f000aa6104514000b000000000000000",
        "name": "<Unnamed>",
        "type": "(Custom Type)",
        "notifiable": false,
        "readable": false,
        "writable": true,
        "writeWithoutResponse": true
    }]
}, {
    "id": "fe8bd5a7.7801b8",
    "type": "ui_group",
    "z": "",
    "name": "Bt-LEDs",
    "tab": "52ae1db3.2ec564",
    "order": 3,
    "disp": true,
    "width": "6",
    "collapse": false
}, {
    "id": "7565343b.be22fc",
    "type": "mqtt-broker",
    "z": "",
    "name": "",
    "broker": "localhost",
    "port": "1883",
    "clientid": "",
    "usetls": false,
    "compatmode": false,
    "keepalive": "60",
    "cleansession": true,
    "birthTopic": "",
    "birthQos": "0",
    "birthPayload": "",
    "closeTopic": "",
    "closeQos": "0",
    "closePayload": "",
    "willTopic": "",
    "willQos": "0",
    "willPayload": ""
}, {
    "id": "52ae1db3.2ec564",
    "type": "ui_tab",
    "z": "",
    "name": "Bt-LEDs",
    "icon": "dashboard",
    "order": 2,
    "disabled": false,
    "hidden": false
}, {
    "id": "1bcfa8d8.054987",
    "type": "Generic BLE",
    "z": "",
    "localName": "YONGNUO LED",
    "address": "ef:ad:99:2e:95:98",
    "uuid": "efad992e9598",
    "muteNotifyEvents": false,
    "operationTimeout": "",
    "characteristics": [{
        "uuid": "2a00",
        "name": "Device Name",
        "type": "org.bluetooth.characteristic.gap.device_name",
        "notifiable": false,
        "readable": true,
        "writable": true,
        "writeWithoutResponse": false
    }, {
        "uuid": "2a01",
        "name": "Appearance",
        "type": "org.bluetooth.characteristic.gap.appearance",
        "notifiable": false,
        "readable": true,
        "writable": false,
        "writeWithoutResponse": false
    }, {
        "uuid": "2a04",
        "name": "Peripheral Preferred Connection Parameters",
        "type": "org.bluetooth.characteristic.gap.peripheral_preferred_connection_parameters",
        "notifiable": false,
        "readable": true,
        "writable": false,
        "writeWithoutResponse": false
    }, {
        "uuid": "2a05",
        "name": "Service Changed",
        "type": "org.bluetooth.characteristic.gatt.service_changed",
        "notifiable": false,
        "readable": false,
        "writable": false,
        "writeWithoutResponse": false
    }, {
        "uuid": "f000aa6304514000b000000000000000",
        "name": "<Unnamed>",
        "type": "(Custom Type)",
        "notifiable": true,
        "readable": false,
        "writable": false,
        "writeWithoutResponse": false
    }, {
        "uuid": "f000aa6104514000b000000000000000",
        "name": "<Unnamed>",
        "type": "(Custom Type)",
        "notifiable": false,
        "readable": false,
        "writable": true,
        "writeWithoutResponse": true
    }]
}, {
    "id": "3236656.d10759a",
    "type": "ui_tab",
    "z": "",
    "name": "Pan Tilt Slider",
    "icon": "dashboard",
    "order": 1,
    "disabled": false,
    "hidden": false
}, {
    "id": "4ba6748.280aa8c",
    "type": "ui_group",
    "z": "",
    "name": "Bt-LEDs",
    "tab": "b48ecb57.12dce8",
    "order": 3,
    "disp": true,
    "width": "6",
    "collapse": false
}, {
    "id": "3848e09.379512",
    "type": "ui_base",
    "theme": {
        "name": "theme-dark",
        "lightTheme": {
            "default": "#0094CE",
            "baseColor": "#0094CE",
            "baseFont": "-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif",
            "edited": true,
            "reset": false
        },
        "darkTheme": {
            "default": "#097479",
            "baseColor": "#b90000",
            "baseFont": "Courier,monospace",
            "edited": true,
            "reset": false
        },
        "customTheme": {
            "name": "Untitled Theme 1",
            "default": "#4B7930",
            "baseColor": "#4B7930",
            "baseFont": "-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif"
        },
        "themeState": {
            "base-color": {
                "default": "#097479",
                "value": "#b90000",
                "edited": true
            },
            "page-titlebar-backgroundColor": {
                "value": "#b90000",
                "edited": false
            },
            "page-backgroundColor": {
                "value": "#111111",
                "edited": false
            },
            "page-sidebar-backgroundColor": {
                "value": "#000000",
                "edited": false
            },
            "group-textColor": {
                "value": "#ff0606",
                "edited": false
            },
            "group-borderColor": {
                "value": "#555555",
                "edited": false
            },
            "group-backgroundColor": {
                "value": "#333333",
                "edited": false
            },
            "widget-textColor": {
                "value": "#eeeeee",
                "edited": false
            },
            "widget-backgroundColor": {
                "value": "#b90000",
                "edited": false
            },
            "widget-borderColor": {
                "value": "#333333",
                "edited": false
            },
            "base-font": {
                "value": "Courier,monospace"
            }
        },
        "angularTheme": {
            "primary": "indigo",
            "accents": "blue",
            "warn": "red",
            "background": "grey"
        }
    },
    "site": {
        "name": "Node-RED Dashboard",
        "hideToolbar": "false",
        "allowSwipe": "false",
        "lockMenu": "false",
        "allowTempTheme": "true",
        "dateFormat": "DD/MM/YYYY",
        "sizes": {
            "sx": 40,
            "sy": 35,
            "gx": 6,
            "gy": 6,
            "cx": 6,
            "cy": 6,
            "px": 0,
            "py": 0
        }
    }
}, {
    "id": "b2bea16b.4dd8d",
    "type": "mqtt-broker",
    "z": "",
    "name": "",
    "broker": "localhost",
    "port": "1883",
    "clientid": "",
    "usetls": false,
    "compatmode": false,
    "keepalive": "60",
    "cleansession": true,
    "birthTopic": "",
    "birthQos": "0",
    "birthPayload": "",
    "closeTopic": "",
    "closeQos": "0",
    "closePayload": "",
    "willTopic": "",
    "willQos": "0",
    "willPayload": ""
}, {
    "id": "ad53d039.25fc",
    "type": "ui_group",
    "z": "",
    "name": "Status",
    "tab": "3236656.d10759a",
    "order": 3,
    "disp": false,
    "width": "7",
    "collapse": false
}, {
    "id": "b48ecb57.12dce8",
    "type": "ui_tab",
    "z": "",
    "name": "Bt-LEDs",
    "icon": "dashboard",
    "order": 2,
    "disabled": false,
    "hidden": false
}, {
    "id": "d4a6b79b.2299a8",
    "type": "ui_group",
    "z": "",
    "name": "Functions",
    "tab": "3236656.d10759a",
    "order": 1,
    "disp": false,
    "width": "3",
    "collapse": false
}, {
    "id": "3f3cf4be.ea33ac",
    "type": "ui_group",
    "z": "",
    "name": "Settings",
    "tab": "3236656.d10759a",
    "order": 4,
    "disp": false,
    "width": 2,
    "collapse": false
}, {
    "id": "201c8997.ed6256",
    "type": "ui_tab",
    "z": "",
    "name": "Keyframe Editor",
    "icon": "dashboard",
    "order": 3,
    "disabled": false,
    "hidden": false
}, {
    "id": "6c9bc8e8.0ad8d8",
    "type": "ui_group",
    "z": "",
    "name": "Controller",
    "tab": "3236656.d10759a",
    "order": 2,
    "disp": false,
    "width": "7",
    "collapse": false
}, {
    "id": "d8df1128.d80cf",
    "type": "ui_group",
    "z": "",
    "name": "Include",
    "tab": "201c8997.ed6256",
    "disp": true,
    "width": "6",
    "collapse": false
}, {
    "id": "16381b45.e2f915",
    "type": "ui_tab",
    "z": "",
    "name": "Controller",
    "icon": "dashboard",
    "disabled": false,
    "hidden": false
}, {
    "id": "f6310671.b6a5c8",
    "type": "ui_group",
    "z": "",
    "name": "Right side",
    "tab": "16381b45.e2f915",
    "order": 3,
    "disp": true,
    "width": "6",
    "collapse": false
}, {
    "id": "768abd2d.911584",
    "type": "ui_group",
    "z": "",
    "name": "Analog",
    "tab": "16381b45.e2f915",
    "order": 2,
    "disp": true,
    "width": "4",
    "collapse": false
}, {
    "id": "e7c1d0cf.0c95d",
    "type": "ui_group",
    "z": "",
    "name": "Left Side",
    "tab": "16381b45.e2f915",
    "order": 1,
    "disp": true,
    "width": "6",
    "collapse": false
}, {
    "id": "29ff3b72.3e3cd4",
    "type": "ui_group",
    "z": "",
    "name": "LCD",
    "tab": "16381b45.e2f915",
    "order": 4,
    "disp": true,
    "width": "4",
    "collapse": false
}, {
    "id": "96b874b1.7737b8",
    "type": "Generic BLE in",
    "z": "6a0d6ea1.57141",
    "name": "",
    "genericBle": "1bcfa8d8.054987",
    "useString": false,
    "notification": true,
    "x": 670,
    "y": 80,
    "wires": [
        ["15a0e956.ba5137"]
    ]
}, {
    "id": "15a0e956.ba5137",
    "type": "debug",
    "z": "6a0d6ea1.57141",
    "name": "",
    "active": true,
    "tosidebar": true,
    "console": false,
    "tostatus": false,
    "complete": "payload",
    "targetType": "msg",
    "x": 920,
    "y": 80,
    "wires": []
}, {
    "id": "1c35db3f.3dd8c5",
    "type": "Generic BLE out",
    "z": "6a0d6ea1.57141",
    "name": "",
    "genericBle": "1bcfa8d8.054987",
    "x": 1030,
    "y": 600,
    "wires": []
}, {
    "id": "9035a02.23d5e6",
    "type": "ui_slider",
    "z": "6a0d6ea1.57141",
    "name": "",
    "label": "warm",
    "tooltip": "",
    "group": "4ba6748.280aa8c",
    "order": 0,
    "width": 0,
    "height": 0,
    "passthru": true,
    "outs": "all",
    "topic": "",
    "min": 0,
    "max": "99",
    "step": 1,
    "x": 350,
    "y": 340,
    "wires": [
        ["82da6f05.02ffa"]
    ]
}, {
    "id": "523f628e.c192ec",
    "type": "ui_slider",
    "z": "6a0d6ea1.57141",
    "name": "",
    "label": "cold",
    "tooltip": "",
    "group": "4ba6748.280aa8c",
    "order": 0,
    "width": 0,
    "height": 0,
    "passthru": true,
    "outs": "all",
    "topic": "",
    "min": 0,
    "max": "99",
    "step": 1,
    "x": 350,
    "y": 400,
    "wires": [
        ["81edbdb6.2163a"]
    ]
}, {
    "id": "81edbdb6.2163a",
    "type": "change",
    "z": "6a0d6ea1.57141",
    "name": "",
    "rules": [{
        "t": "set",
        "p": "cold",
        "pt": "flow",
        "to": "payload",
        "tot": "msg"
    }],
    "action": "",
    "property": "",
    "from": "",
    "to": "",
    "reg": false,
    "x": 560,
    "y": 400,
    "wires": [
        ["dc2fd41f.453e48"]
    ]
}, {
    "id": "efeefc78.88a61",
    "type": "ui_switch",
    "z": "6a0d6ea1.57141",
    "name": "",
    "label": "ON / OFF",
    "tooltip": "",
    "group": "4ba6748.280aa8c",
    "order": 2,
    "width": 0,
    "height": 0,
    "passthru": true,
    "decouple": "false",
    "topic": "",
    "style": "",
    "onvalue": "aa",
    "onvalueType": "str",
    "onicon": "",
    "oncolor": "",
    "offvalue": "ee",
    "offvalueType": "str",
    "officon": "",
    "offcolor": "",
    "x": 360,
    "y": 520,
    "wires": [
        ["3c51f6a5.68aeaa"]
    ]
}, {
    "id": "3c51f6a5.68aeaa",
    "type": "change",
    "z": "6a0d6ea1.57141",
    "name": "",
    "rules": [{
        "t": "set",
        "p": "on_off",
        "pt": "flow",
        "to": "payload",
        "tot": "msg"
    }],
    "action": "",
    "property": "",
    "from": "",
    "to": "",
    "reg": false,
    "x": 567,
    "y": 519,
    "wires": [
        ["dc2fd41f.453e48"]
    ]
}, {
    "id": "dc2fd41f.453e48",
    "type": "function",
    "z": "6a0d6ea1.57141",
    "name": "",
    "func": "if (flow.get(\"on_off\") === undefined){\n    flow.set(\"on_off\", \"ee\")\n}\nif (flow.get(\"cold\") === undefined){\n    flow.set(\"cold\", \"3f\")\n}\nif (flow.get(\"warm\") === undefined){\n    flow.set(\"warm\", \"3f\")\n}\nif (flow.get(\"channel\") === undefined){\n    flow.set(\"channel\", \"01\")\n}\nvar message = \"ae\"\nmessage += flow.get(\"on_off\")\nmessage += (\"0\" + flow.get(\"channel\").toString(16)).substr(-2)\nmessage += (\"0\" + flow.get(\"cold\").toString(16)).substr(-2)\nmessage += (\"0\" + flow.get(\"warm\").toString(16)).substr(-2)\nmessage += \"56\"\n\nmsg.payload = {\n    \"f000aa6104514000b000000000000000\" : message\n}\n\nreturn msg;",
    "outputs": 1,
    "noerr": 0,
    "x": 810,
    "y": 520,
    "wires": [
        ["2cb4f39a.688d5c", "1c35db3f.3dd8c5"]
    ]
}, {
    "id": "2cb4f39a.688d5c",
    "type": "debug",
    "z": "6a0d6ea1.57141",
    "name": "",
    "active": true,
    "tosidebar": true,
    "console": false,
    "tostatus": false,
    "complete": "payload",
    "targetType": "msg",
    "x": 1030,
    "y": 520,
    "wires": []
}, {
    "id": "33722788.9f7e58",
    "type": "ui_slider",
    "z": "6a0d6ea1.57141",
    "name": "",
    "label": "channel",
    "tooltip": "",
    "group": "4ba6748.280aa8c",
    "order": 0,
    "width": 0,
    "height": 0,
    "passthru": true,
    "outs": "end",
    "topic": "",
    "min": "1",
    "max": "8",
    "step": 1,
    "x": 360,
    "y": 460,
    "wires": [
        ["17733a12.545086"]
    ]
}, {
    "id": "17733a12.545086",
    "type": "change",
    "z": "6a0d6ea1.57141",
    "name": "",
    "rules": [{
        "t": "set",
        "p": "channel",
        "pt": "flow",
        "to": "payload",
        "tot": "msg"
    }],
    "action": "",
    "property": "",
    "from": "",
    "to": "",
    "reg": false,
    "x": 570,
    "y": 460,
    "wires": [
        ["dc2fd41f.453e48"]
    ]
}, {
    "id": "e31c958e.581798",
    "type": "mqtt in",
    "z": "6a0d6ea1.57141",
    "name": "",
    "topic": "onoff",
    "qos": "0",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 150,
    "y": 520,
    "wires": [
        ["efeefc78.88a61"]
    ]
}, {
    "id": "95b398c4.22e708",
    "type": "mqtt in",
    "z": "6a0d6ea1.57141",
    "name": "",
    "topic": "warm",
    "qos": "0",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 150,
    "y": 340,
    "wires": [
        ["9035a02.23d5e6"]
    ]
}, {
    "id": "acaa1db4.5dacf",
    "type": "mqtt in",
    "z": "6a0d6ea1.57141",
    "name": "",
    "topic": "cold",
    "qos": "0",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 150,
    "y": 400,
    "wires": [
        ["523f628e.c192ec"]
    ]
}, {
    "id": "6cbac8ab.d9e828",
    "type": "mqtt in",
    "z": "6a0d6ea1.57141",
    "name": "",
    "topic": "channel",
    "qos": "0",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 150,
    "y": 460,
    "wires": [
        ["33722788.9f7e58"]
    ]
}, {
    "id": "339695e7.d8ab3a",
    "type": "ui_slider",
    "z": "7820a8e.84a8458",
    "name": "",
    "label": "Max jog",
    "tooltip": "",
    "group": "3f3cf4be.ea33ac",
    "order": 1,
    "width": 2,
    "height": "7",
    "passthru": false,
    "outs": "end",
    "topic": "",
    "min": "750",
    "max": "10000",
    "step": "50",
    "x": 620,
    "y": 80,
    "wires": [
        ["d1d74925.2f5b08"]
    ]
}, {
    "id": "70c16460.696bac",
    "type": "mqtt in",
    "z": "7820a8e.84a8458",
    "name": "",
    "topic": "panTiltSlider/settings/grbl/maxJogSpeed/R",
    "qos": "0",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 300,
    "y": 80,
    "wires": [
        ["339695e7.d8ab3a"]
    ]
}, {
    "id": "d1d74925.2f5b08",
    "type": "mqtt out",
    "z": "7820a8e.84a8458",
    "name": "",
    "topic": "panTiltSlider/settings/grbl/maxJogSpeed/W",
    "qos": "",
    "retain": "",
    "broker": "b2bea16b.4dd8d",
    "x": 950,
    "y": 80,
    "wires": []
}, {
    "id": "b22a2a26.c68598",
    "type": "debug",
    "z": "7820a8e.84a8458",
    "name": "",
    "active": false,
    "tosidebar": true,
    "console": false,
    "tostatus": false,
    "complete": "false",
    "x": 1070,
    "y": 180,
    "wires": []
}, {
    "id": "e68bed0b.06f3c",
    "type": "mqtt in",
    "z": "7820a8e.84a8458",
    "name": "",
    "topic": "controller/#",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 810,
    "y": 180,
    "wires": [
        ["b22a2a26.c68598"]
    ]
}, {
    "id": "82f129ad.1e98a8",
    "type": "mqtt in",
    "z": "25436c92.733984",
    "name": "",
    "topic": "panTiltSlider/status/grbl/status/R",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 390,
    "y": 100,
    "wires": [
        ["4b62865e.e57f38", "84e46d73.85bed"]
    ]
}, {
    "id": "4b62865e.e57f38",
    "type": "ui_text",
    "z": "25436c92.733984",
    "group": "ad53d039.25fc",
    "order": 1,
    "width": 5,
    "height": 1,
    "name": "",
    "label": "Status",
    "format": "{{msg.payload}}",
    "layout": "row-spread",
    "x": 610,
    "y": 100,
    "wires": []
}, {
    "id": "b40e586.9b1c6a8",
    "type": "mqtt in",
    "z": "25436c92.733984",
    "name": "",
    "topic": "panTiltSlider/status/grbl/x/R",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 400,
    "y": 300,
    "wires": [
        ["d47f3328.e6629"]
    ]
}, {
    "id": "d47f3328.e6629",
    "type": "ui_text",
    "z": "25436c92.733984",
    "group": "6c9bc8e8.0ad8d8",
    "order": 3,
    "width": 3,
    "height": 1,
    "name": "",
    "label": "X",
    "format": "{{msg.payload}}",
    "layout": "row-spread",
    "x": 610,
    "y": 300,
    "wires": []
}, {
    "id": "b3ce40da.30257",
    "type": "mqtt in",
    "z": "25436c92.733984",
    "name": "",
    "topic": "panTiltSlider/status/grbl/y/R",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 400,
    "y": 360,
    "wires": [
        ["568ac063.2076"]
    ]
}, {
    "id": "568ac063.2076",
    "type": "ui_text",
    "z": "25436c92.733984",
    "group": "6c9bc8e8.0ad8d8",
    "order": 6,
    "width": 3,
    "height": 1,
    "name": "",
    "label": "Y",
    "format": "{{msg.payload}}",
    "layout": "row-spread",
    "x": 610,
    "y": 360,
    "wires": []
}, {
    "id": "b00c50aa.e138f",
    "type": "mqtt in",
    "z": "25436c92.733984",
    "name": "",
    "topic": "panTiltSlider/status/grbl/z/R",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 400,
    "y": 420,
    "wires": [
        ["ada42296.97de4"]
    ]
}, {
    "id": "ada42296.97de4",
    "type": "ui_text",
    "z": "25436c92.733984",
    "group": "6c9bc8e8.0ad8d8",
    "order": 7,
    "width": 3,
    "height": 1,
    "name": "",
    "label": "Z",
    "format": "{{msg.payload}}",
    "layout": "row-spread",
    "x": 610,
    "y": 420,
    "wires": []
}, {
    "id": "eeffabe6.c0ae98",
    "type": "mqtt in",
    "z": "25436c92.733984",
    "name": "",
    "topic": "panTiltSlider/status/grbl/error/message/R",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 360,
    "y": 220,
    "wires": [
        ["5439b22f.6ffbdc", "b7c9ef84.7d035"]
    ]
}, {
    "id": "5439b22f.6ffbdc",
    "type": "ui_text",
    "z": "25436c92.733984",
    "group": "ad53d039.25fc",
    "order": 5,
    "width": 5,
    "height": 1,
    "name": "",
    "label": "Error",
    "format": "{{msg.payload}}",
    "layout": "row-spread",
    "x": 610,
    "y": 220,
    "wires": []
}, {
    "id": "f9481087.23606",
    "type": "mqtt in",
    "z": "25436c92.733984",
    "name": "",
    "topic": "panTiltSlider/status/grbl/alarm/message/R",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 360,
    "y": 160,
    "wires": [
        ["2426f313.89e77c", "1893bfd2.ef2c9"]
    ]
}, {
    "id": "2426f313.89e77c",
    "type": "ui_text",
    "z": "25436c92.733984",
    "group": "ad53d039.25fc",
    "order": 3,
    "width": 5,
    "height": 1,
    "name": "",
    "label": "Alarm",
    "format": "{{msg.payload}}",
    "layout": "row-spread",
    "x": 610,
    "y": 160,
    "wires": []
}, {
    "id": "84498ab2.c9c5a8",
    "type": "mqtt out",
    "z": "440cacce.57aac4",
    "name": "",
    "topic": "panTiltSlider/functions/grbl/killAlarmLock/W",
    "qos": "",
    "retain": "",
    "broker": "b2bea16b.4dd8d",
    "x": 650,
    "y": 140,
    "wires": []
}, {
    "id": "4e9935de.9d9f9c",
    "type": "ui_button",
    "z": "440cacce.57aac4",
    "name": "",
    "group": "d4a6b79b.2299a8",
    "order": 1,
    "width": 0,
    "height": 0,
    "passthru": false,
    "label": "Kill Alarm",
    "tooltip": "",
    "color": "",
    "bgcolor": "",
    "icon": "",
    "payload": "",
    "payloadType": "str",
    "topic": "",
    "x": 360,
    "y": 140,
    "wires": [
        ["84498ab2.c9c5a8"]
    ]
}, {
    "id": "e9082247.c8e88",
    "type": "mqtt out",
    "z": "440cacce.57aac4",
    "name": "",
    "topic": "panTiltSlider/functions/grbl/softReset/W",
    "qos": "",
    "retain": "",
    "broker": "b2bea16b.4dd8d",
    "x": 640,
    "y": 200,
    "wires": []
}, {
    "id": "bc233e37.a8d17",
    "type": "ui_button",
    "z": "440cacce.57aac4",
    "name": "",
    "group": "d4a6b79b.2299a8",
    "order": 2,
    "width": 0,
    "height": 0,
    "passthru": false,
    "label": "Soft Reset",
    "tooltip": "",
    "color": "",
    "bgcolor": "",
    "icon": "",
    "payload": "",
    "payloadType": "str",
    "topic": "",
    "x": 350,
    "y": 200,
    "wires": [
        ["e9082247.c8e88"]
    ]
}, {
    "id": "9dab2191.62839",
    "type": "mqtt out",
    "z": "440cacce.57aac4",
    "name": "",
    "topic": "panTiltSlider/functions/grbl/resume/W",
    "qos": "",
    "retain": "",
    "broker": "b2bea16b.4dd8d",
    "x": 630,
    "y": 260,
    "wires": []
}, {
    "id": "b9651732.20cbc8",
    "type": "ui_button",
    "z": "440cacce.57aac4",
    "name": "",
    "group": "d4a6b79b.2299a8",
    "order": 3,
    "width": 0,
    "height": 0,
    "passthru": false,
    "label": "Resume",
    "tooltip": "",
    "color": "",
    "bgcolor": "",
    "icon": "",
    "payload": "",
    "payloadType": "str",
    "topic": "",
    "x": 360,
    "y": 260,
    "wires": [
        ["9dab2191.62839"]
    ]
}, {
    "id": "86f47344.edbae",
    "type": "mqtt out",
    "z": "440cacce.57aac4",
    "name": "",
    "topic": "panTiltSlider/functions/keyframeEditor/playAllKeyframes/W",
    "qos": "",
    "retain": "",
    "broker": "b2bea16b.4dd8d",
    "x": 700,
    "y": 320,
    "wires": []
}, {
    "id": "bcd39d06.c8f99",
    "type": "ui_button",
    "z": "440cacce.57aac4",
    "name": "",
    "group": "d4a6b79b.2299a8",
    "order": 4,
    "width": 0,
    "height": 0,
    "passthru": false,
    "label": "Play Keyframes",
    "tooltip": "",
    "color": "",
    "bgcolor": "",
    "icon": "",
    "payload": "",
    "payloadType": "str",
    "topic": "",
    "x": 320,
    "y": 320,
    "wires": [
        ["86f47344.edbae"]
    ]
}, {
    "id": "931ee67.2308418",
    "type": "mqtt out",
    "z": "440cacce.57aac4",
    "name": "",
    "topic": "panTiltSlider/functions/grbl/feedHold/W",
    "qos": "",
    "retain": "",
    "broker": "b2bea16b.4dd8d",
    "x": 630,
    "y": 380,
    "wires": []
}, {
    "id": "254d0b22.38f854",
    "type": "ui_button",
    "z": "440cacce.57aac4",
    "name": "",
    "group": "d4a6b79b.2299a8",
    "order": 5,
    "width": 0,
    "height": 0,
    "passthru": false,
    "label": "Feed Hold",
    "tooltip": "",
    "color": "",
    "bgcolor": "",
    "icon": "",
    "payload": "",
    "payloadType": "str",
    "topic": "",
    "x": 350,
    "y": 380,
    "wires": [
        ["931ee67.2308418"]
    ]
}, {
    "id": "f80a71a8.f4fb4",
    "type": "mqtt out",
    "z": "440cacce.57aac4",
    "name": "",
    "topic": "panTiltSlider/functions/grbl/jogCancel/W",
    "qos": "",
    "retain": "",
    "broker": "b2bea16b.4dd8d",
    "x": 640,
    "y": 440,
    "wires": []
}, {
    "id": "7fa3bf46.49b2e",
    "type": "ui_button",
    "z": "440cacce.57aac4",
    "name": "",
    "group": "d4a6b79b.2299a8",
    "order": 6,
    "width": 0,
    "height": 0,
    "passthru": false,
    "label": "Jog Cancel",
    "tooltip": "",
    "color": "",
    "bgcolor": "",
    "icon": "",
    "payload": "",
    "payloadType": "str",
    "topic": "",
    "x": 350,
    "y": 440,
    "wires": [
        ["f80a71a8.f4fb4"]
    ]
}, {
    "id": "e5165ac5.9a9968",
    "type": "mqtt in",
    "z": "25436c92.733984",
    "name": "",
    "topic": "controller/RX/event",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 430,
    "y": 580,
    "wires": [
        ["4a450980.7853e8"]
    ]
}, {
    "id": "7ad1a7c1.4d4c98",
    "type": "mqtt in",
    "z": "25436c92.733984",
    "name": "",
    "topic": "controller/RY/event",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 430,
    "y": 640,
    "wires": [
        ["deb44cee.6a4b6"]
    ]
}, {
    "id": "e4bfa0f7.0b647",
    "type": "mqtt in",
    "z": "25436c92.733984",
    "name": "",
    "topic": "controller/LX/event",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 430,
    "y": 700,
    "wires": [
        ["e3d56ebd.a6d04"]
    ]
}, {
    "id": "4a450980.7853e8",
    "type": "ui_level",
    "z": "25436c92.733984",
    "group": "6c9bc8e8.0ad8d8",
    "order": 4,
    "width": 1,
    "height": "4",
    "name": "RX",
    "label": "RX",
    "colorHi": "#e60000",
    "colorWarn": "#ff9900",
    "colorNormal": "#00b33c",
    "colorOff": "#595959",
    "min": "-1",
    "max": "1",
    "segWarn": "",
    "segHigh": "",
    "unit": "",
    "layout": "sv",
    "channelA": "",
    "channelB": "",
    "decimals": "2",
    "animations": "reactive",
    "shape": "2",
    "colorschema": "fixed",
    "textoptions": "default",
    "colorText": "#eeeeee",
    "fontLabel": "",
    "fontValue": "",
    "fontSmall": "",
    "colorFromTheme": true,
    "textAnimations": true,
    "hideValue": false,
    "tickmode": "off",
    "peakmode": false,
    "peaktime": 3000,
    "x": 610,
    "y": 580,
    "wires": []
}, {
    "id": "e3d56ebd.a6d04",
    "type": "ui_level",
    "z": "25436c92.733984",
    "group": "6c9bc8e8.0ad8d8",
    "order": 1,
    "width": 1,
    "height": "4",
    "name": "LX",
    "label": "LX",
    "colorHi": "#e60000",
    "colorWarn": "#ff9900",
    "colorNormal": "#00b33c",
    "colorOff": "#595959",
    "min": "-1",
    "max": "1",
    "segWarn": "",
    "segHigh": "",
    "unit": "",
    "layout": "sv",
    "channelA": "",
    "channelB": "",
    "decimals": "2",
    "animations": "reactive",
    "shape": "2",
    "colorschema": "fixed",
    "textoptions": "default",
    "colorText": "#eeeeee",
    "fontLabel": "",
    "fontValue": "",
    "fontSmall": "",
    "colorFromTheme": true,
    "textAnimations": true,
    "hideValue": false,
    "tickmode": "off",
    "peakmode": false,
    "peaktime": 3000,
    "x": 610,
    "y": 700,
    "wires": []
}, {
    "id": "deb44cee.6a4b6",
    "type": "ui_level",
    "z": "25436c92.733984",
    "group": "6c9bc8e8.0ad8d8",
    "order": 5,
    "width": 1,
    "height": "4",
    "name": "RY",
    "label": "RY",
    "colorHi": "#e60000",
    "colorWarn": "#ff9900",
    "colorNormal": "#00b33c",
    "colorOff": "#595959",
    "min": "-1",
    "max": "1",
    "segWarn": "",
    "segHigh": "",
    "unit": "",
    "layout": "sv",
    "channelA": "",
    "channelB": "",
    "decimals": "2",
    "animations": "reactive",
    "shape": 2,
    "colorschema": "fixed",
    "textoptions": "default",
    "colorText": "#eeeeee",
    "fontLabel": "",
    "fontValue": "",
    "fontSmall": "",
    "colorFromTheme": true,
    "textAnimations": true,
    "hideValue": false,
    "tickmode": "off",
    "peakmode": false,
    "peaktime": 3000,
    "x": 610,
    "y": 640,
    "wires": []
}, {
    "id": "84e46d73.85bed",
    "type": "ui_led",
    "z": "25436c92.733984",
    "group": "ad53d039.25fc",
    "order": 2,
    "width": 2,
    "height": 1,
    "label": "",
    "labelPlacement": "left",
    "labelAlignment": "left",
    "colorForValue": [{
        "color": "green",
        "value": "Idle",
        "valueType": "str"
    }, {
        "color": "yellow",
        "value": "Jog",
        "valueType": "str"
    }, {
        "color": "blue",
        "value": "Run",
        "valueType": "str"
    }, {
        "color": "red",
        "value": "Alarm",
        "valueType": "str"
    }],
    "allowColorForValueInMessage": false,
    "name": "Status",
    "x": 890,
    "y": 100,
    "wires": []
}, {
    "id": "75744a5e.d31944",
    "type": "ui_led",
    "z": "25436c92.733984",
    "group": "ad53d039.25fc",
    "order": 4,
    "width": 2,
    "height": 1,
    "label": "",
    "labelPlacement": "left",
    "labelAlignment": "left",
    "colorForValue": [{
        "color": "green",
        "value": "true",
        "valueType": "bool"
    }, {
        "color": "red",
        "value": "false",
        "valueType": "bool"
    }],
    "allowColorForValueInMessage": false,
    "name": "Alarm",
    "x": 890,
    "y": 160,
    "wires": []
}, {
    "id": "1893bfd2.ef2c9",
    "type": "function",
    "z": "25436c92.733984",
    "name": "",
    "func": "msg.payload = msg.payload == \"\"\nreturn msg;",
    "outputs": 1,
    "noerr": 0,
    "x": 740,
    "y": 160,
    "wires": [
        ["75744a5e.d31944"]
    ]
}, {
    "id": "6d400c01.e25ee4",
    "type": "ui_led",
    "z": "25436c92.733984",
    "group": "ad53d039.25fc",
    "order": 6,
    "width": 2,
    "height": 1,
    "label": "",
    "labelPlacement": "left",
    "labelAlignment": "left",
    "colorForValue": [{
        "color": "green",
        "value": "true",
        "valueType": "bool"
    }, {
        "color": "red",
        "value": "false",
        "valueType": "bool"
    }],
    "allowColorForValueInMessage": false,
    "name": "Error",
    "x": 890,
    "y": 220,
    "wires": []
}, {
    "id": "b7c9ef84.7d035",
    "type": "function",
    "z": "25436c92.733984",
    "name": "",
    "func": "msg.payload = msg.payload == \"\"\nreturn msg;",
    "outputs": 1,
    "noerr": 0,
    "x": 750,
    "y": 220,
    "wires": [
        ["6d400c01.e25ee4"]
    ]
}, {
    "id": "33b0e1d3.3f2d8e",
    "type": "mqtt in",
    "z": "25436c92.733984",
    "name": "",
    "topic": "controller/LY/event",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 430,
    "y": 760,
    "wires": [
        ["a6310044.ea674"]
    ]
}, {
    "id": "a6310044.ea674",
    "type": "ui_level",
    "z": "25436c92.733984",
    "group": "6c9bc8e8.0ad8d8",
    "order": 2,
    "width": 1,
    "height": "4",
    "name": "LY",
    "label": "LY",
    "colorHi": "#e60000",
    "colorWarn": "#ff9900",
    "colorNormal": "#00b33c",
    "colorOff": "#595959",
    "min": "-1",
    "max": "1",
    "segWarn": "",
    "segHigh": "",
    "unit": "",
    "layout": "sv",
    "channelA": "",
    "channelB": "",
    "decimals": "2",
    "animations": "reactive",
    "shape": "2",
    "colorschema": "fixed",
    "textoptions": "default",
    "colorText": "#eeeeee",
    "fontLabel": "",
    "fontValue": "",
    "fontSmall": "",
    "colorFromTheme": true,
    "textAnimations": true,
    "hideValue": false,
    "tickmode": "off",
    "peakmode": false,
    "peaktime": 3000,
    "x": 610,
    "y": 760,
    "wires": []
}, {
    "id": "57327786.108778",
    "type": "mqtt in",
    "z": "6684ba10.301104",
    "name": "",
    "topic": "panTiltSlider/status/keyframeEditor/includeA/R",
    "qos": "0",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 290,
    "y": 220,
    "wires": [
        ["bb88c229.6ac35"]
    ]
}, {
    "id": "f829c412.5d6cb8",
    "type": "mqtt out",
    "z": "6684ba10.301104",
    "name": "",
    "topic": "panTiltSlider/functions/keyframeEditor/toggleIncludeA/W",
    "qos": "",
    "retain": "",
    "broker": "b2bea16b.4dd8d",
    "x": 970,
    "y": 220,
    "wires": []
}, {
    "id": "bb88c229.6ac35",
    "type": "ui_switch",
    "z": "6684ba10.301104",
    "name": "",
    "label": "A",
    "tooltip": "",
    "group": "d8df1128.d80cf",
    "order": 0,
    "width": 0,
    "height": 0,
    "passthru": false,
    "decouple": "true",
    "topic": "",
    "style": "",
    "onvalue": "true",
    "onvalueType": "str",
    "onicon": "",
    "oncolor": "",
    "offvalue": "false",
    "offvalueType": "str",
    "officon": "",
    "offcolor": "",
    "x": 630,
    "y": 220,
    "wires": [
        ["f829c412.5d6cb8"]
    ]
}, {
    "id": "86f1a38b.1a16f",
    "type": "mqtt in",
    "z": "6684ba10.301104",
    "name": "",
    "topic": "panTiltSlider/status/keyframeEditor/includeB/R",
    "qos": "0",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 290,
    "y": 280,
    "wires": [
        ["b8c87cd.afdba8"]
    ]
}, {
    "id": "6b20e6ad.481238",
    "type": "mqtt out",
    "z": "6684ba10.301104",
    "name": "",
    "topic": "panTiltSlider/functions/keyframeEditor/toggleIncludeB/W",
    "qos": "",
    "retain": "",
    "broker": "b2bea16b.4dd8d",
    "x": 970,
    "y": 280,
    "wires": []
}, {
    "id": "b8c87cd.afdba8",
    "type": "ui_switch",
    "z": "6684ba10.301104",
    "name": "",
    "label": "B",
    "tooltip": "",
    "group": "d8df1128.d80cf",
    "order": 0,
    "width": 0,
    "height": 0,
    "passthru": false,
    "decouple": "true",
    "topic": "",
    "style": "",
    "onvalue": "true",
    "onvalueType": "str",
    "onicon": "",
    "oncolor": "",
    "offvalue": "false",
    "offvalueType": "str",
    "officon": "",
    "offcolor": "",
    "x": 630,
    "y": 280,
    "wires": [
        ["6b20e6ad.481238"]
    ]
}, {
    "id": "db1da177.88c2e",
    "type": "mqtt in",
    "z": "6684ba10.301104",
    "name": "",
    "topic": "panTiltSlider/status/keyframeEditor/includeX/R",
    "qos": "0",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 290,
    "y": 340,
    "wires": [
        ["b898f6b8.4bbba8"]
    ]
}, {
    "id": "3f870fde.ecfb2",
    "type": "mqtt out",
    "z": "6684ba10.301104",
    "name": "",
    "topic": "panTiltSlider/functions/keyframeEditor/toggleIncludeX/W",
    "qos": "",
    "retain": "",
    "broker": "b2bea16b.4dd8d",
    "x": 970,
    "y": 340,
    "wires": []
}, {
    "id": "b898f6b8.4bbba8",
    "type": "ui_switch",
    "z": "6684ba10.301104",
    "name": "",
    "label": "X",
    "tooltip": "",
    "group": "d8df1128.d80cf",
    "order": 0,
    "width": 0,
    "height": 0,
    "passthru": false,
    "decouple": "true",
    "topic": "",
    "style": "",
    "onvalue": "true",
    "onvalueType": "str",
    "onicon": "",
    "oncolor": "",
    "offvalue": "false",
    "offvalueType": "str",
    "officon": "",
    "offcolor": "",
    "x": 630,
    "y": 340,
    "wires": [
        ["3f870fde.ecfb2"]
    ]
}, {
    "id": "d12f7eac.90595",
    "type": "mqtt in",
    "z": "6684ba10.301104",
    "name": "",
    "topic": "panTiltSlider/status/keyframeEditor/includeY/R",
    "qos": "0",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 290,
    "y": 400,
    "wires": [
        ["14efeab3.e67245"]
    ]
}, {
    "id": "9cbd03ea.78c12",
    "type": "mqtt out",
    "z": "6684ba10.301104",
    "name": "",
    "topic": "panTiltSlider/functions/keyframeEditor/toggleIncludeY/W",
    "qos": "",
    "retain": "",
    "broker": "b2bea16b.4dd8d",
    "x": 970,
    "y": 400,
    "wires": []
}, {
    "id": "14efeab3.e67245",
    "type": "ui_switch",
    "z": "6684ba10.301104",
    "name": "",
    "label": "Y",
    "tooltip": "",
    "group": "d8df1128.d80cf",
    "order": 0,
    "width": 0,
    "height": 0,
    "passthru": false,
    "decouple": "true",
    "topic": "",
    "style": "",
    "onvalue": "true",
    "onvalueType": "str",
    "onicon": "",
    "oncolor": "",
    "offvalue": "false",
    "offvalueType": "str",
    "officon": "",
    "offcolor": "",
    "x": 630,
    "y": 400,
    "wires": [
        ["9cbd03ea.78c12"]
    ]
}, {
    "id": "ce3030a.5679ed",
    "type": "mqtt in",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/RX/event",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 530,
    "y": 60,
    "wires": [
        ["5bee316b.59a39"]
    ]
}, {
    "id": "9e01750c.7d1fe8",
    "type": "mqtt in",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/RY/event",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 530,
    "y": 120,
    "wires": [
        ["5fff5e56.f28b4"]
    ]
}, {
    "id": "70b5fbc5.3ed324",
    "type": "mqtt in",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/LX/event",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 530,
    "y": 180,
    "wires": [
        ["2f381009.98662"]
    ]
}, {
    "id": "5bee316b.59a39",
    "type": "ui_level",
    "z": "16dba669.90683a",
    "group": "768abd2d.911584",
    "order": 4,
    "width": 1,
    "height": 3,
    "name": "RX",
    "label": "RX",
    "colorHi": "#e60000",
    "colorWarn": "#ff9900",
    "colorNormal": "#00b33c",
    "colorOff": "#595959",
    "min": "-1",
    "max": "1",
    "segWarn": "",
    "segHigh": "",
    "unit": "",
    "layout": "sv",
    "channelA": "",
    "channelB": "",
    "decimals": "2",
    "animations": "reactive",
    "shape": "2",
    "colorschema": "fixed",
    "textoptions": "default",
    "colorText": "#eeeeee",
    "fontLabel": "",
    "fontValue": "",
    "fontSmall": "",
    "colorFromTheme": true,
    "textAnimations": true,
    "hideValue": false,
    "tickmode": "off",
    "peakmode": false,
    "peaktime": 3000,
    "x": 710,
    "y": 60,
    "wires": []
}, {
    "id": "2f381009.98662",
    "type": "ui_level",
    "z": "16dba669.90683a",
    "group": "768abd2d.911584",
    "order": 1,
    "width": 1,
    "height": 3,
    "name": "LX",
    "label": "LX",
    "colorHi": "#e60000",
    "colorWarn": "#ff9900",
    "colorNormal": "#00b33c",
    "colorOff": "#595959",
    "min": "-1",
    "max": "1",
    "segWarn": "",
    "segHigh": "",
    "unit": "",
    "layout": "sv",
    "channelA": "",
    "channelB": "",
    "decimals": "2",
    "animations": "reactive",
    "shape": "2",
    "colorschema": "fixed",
    "textoptions": "default",
    "colorText": "#eeeeee",
    "fontLabel": "",
    "fontValue": "",
    "fontSmall": "",
    "colorFromTheme": true,
    "textAnimations": true,
    "hideValue": false,
    "tickmode": "off",
    "peakmode": false,
    "peaktime": 3000,
    "x": 710,
    "y": 180,
    "wires": []
}, {
    "id": "5fff5e56.f28b4",
    "type": "ui_level",
    "z": "16dba669.90683a",
    "group": "768abd2d.911584",
    "order": 5,
    "width": 1,
    "height": 3,
    "name": "RY",
    "label": "RY",
    "colorHi": "#e60000",
    "colorWarn": "#ff9900",
    "colorNormal": "#00b33c",
    "colorOff": "#595959",
    "min": "-1",
    "max": "1",
    "segWarn": "",
    "segHigh": "",
    "unit": "",
    "layout": "sv",
    "channelA": "",
    "channelB": "",
    "decimals": "2",
    "animations": "reactive",
    "shape": 2,
    "colorschema": "fixed",
    "textoptions": "default",
    "colorText": "#eeeeee",
    "fontLabel": "",
    "fontValue": "",
    "fontSmall": "",
    "colorFromTheme": true,
    "textAnimations": true,
    "hideValue": false,
    "tickmode": "off",
    "peakmode": false,
    "peaktime": 3000,
    "x": 710,
    "y": 120,
    "wires": []
}, {
    "id": "fe651d1d.46714",
    "type": "mqtt in",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/LY/event",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 530,
    "y": 240,
    "wires": [
        ["adeef019.3bd85"]
    ]
}, {
    "id": "adeef019.3bd85",
    "type": "ui_level",
    "z": "16dba669.90683a",
    "group": "768abd2d.911584",
    "order": 2,
    "width": 1,
    "height": 3,
    "name": "LY",
    "label": "LY",
    "colorHi": "#e60000",
    "colorWarn": "#ff9900",
    "colorNormal": "#00b33c",
    "colorOff": "#595959",
    "min": "-1",
    "max": "1",
    "segWarn": "",
    "segHigh": "",
    "unit": "",
    "layout": "sv",
    "channelA": "",
    "channelB": "",
    "decimals": "2",
    "animations": "reactive",
    "shape": "2",
    "colorschema": "fixed",
    "textoptions": "default",
    "colorText": "#eeeeee",
    "fontLabel": "",
    "fontValue": "",
    "fontSmall": "",
    "colorFromTheme": true,
    "textAnimations": true,
    "hideValue": false,
    "tickmode": "off",
    "peakmode": false,
    "peaktime": 3000,
    "x": 710,
    "y": 240,
    "wires": []
}, {
    "id": "a7928b4.db5f878",
    "type": "mqtt in",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/A/read/value",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 900,
    "y": 60,
    "wires": [
        ["2ba1b919.40c336"]
    ]
}, {
    "id": "2ba1b919.40c336",
    "type": "ui_led",
    "z": "16dba669.90683a",
    "group": "f6310671.b6a5c8",
    "order": 1,
    "width": "3",
    "height": "1",
    "label": "A",
    "labelPlacement": "left",
    "labelAlignment": "right",
    "colorForValue": [{
        "color": "red",
        "value": "0",
        "valueType": "str"
    }, {
        "color": "green",
        "value": "1",
        "valueType": "str"
    }],
    "allowColorForValueInMessage": false,
    "name": "",
    "x": 1110,
    "y": 60,
    "wires": []
}, {
    "id": "a3953b04.f7df58",
    "type": "mqtt in",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/B/read/value",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 900,
    "y": 120,
    "wires": [
        ["f8ac22e1.391d7"]
    ]
}, {
    "id": "f8ac22e1.391d7",
    "type": "ui_led",
    "z": "16dba669.90683a",
    "group": "f6310671.b6a5c8",
    "order": 2,
    "width": "3",
    "height": "1",
    "label": "B",
    "labelPlacement": "left",
    "labelAlignment": "right",
    "colorForValue": [{
        "color": "red",
        "value": "0",
        "valueType": "str"
    }, {
        "color": "green",
        "value": "1",
        "valueType": "str"
    }],
    "allowColorForValueInMessage": false,
    "name": "",
    "x": 1110,
    "y": 120,
    "wires": []
}, {
    "id": "f6a87f5b.2bedb",
    "type": "mqtt in",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/X/read/value",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 900,
    "y": 180,
    "wires": [
        ["e240dbd8.a6c1d8"]
    ]
}, {
    "id": "e240dbd8.a6c1d8",
    "type": "ui_led",
    "z": "16dba669.90683a",
    "group": "f6310671.b6a5c8",
    "order": 3,
    "width": "3",
    "height": "1",
    "label": "X",
    "labelPlacement": "left",
    "labelAlignment": "right",
    "colorForValue": [{
        "color": "red",
        "value": "0",
        "valueType": "str"
    }, {
        "color": "green",
        "value": "1",
        "valueType": "str"
    }],
    "allowColorForValueInMessage": false,
    "name": "",
    "x": 1110,
    "y": 180,
    "wires": []
}, {
    "id": "3dccc2f.64e8f3e",
    "type": "mqtt in",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/Y/read/value",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 900,
    "y": 240,
    "wires": [
        ["65c9d309.459e8c"]
    ]
}, {
    "id": "65c9d309.459e8c",
    "type": "ui_led",
    "z": "16dba669.90683a",
    "group": "f6310671.b6a5c8",
    "order": 4,
    "width": "3",
    "height": "1",
    "label": "Y",
    "labelPlacement": "left",
    "labelAlignment": "right",
    "colorForValue": [{
        "color": "red",
        "value": "0",
        "valueType": "str"
    }, {
        "color": "green",
        "value": "1",
        "valueType": "str"
    }],
    "allowColorForValueInMessage": false,
    "name": "",
    "x": 1110,
    "y": 240,
    "wires": []
}, {
    "id": "80310dcc.c812e",
    "type": "mqtt in",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/R1/read/value",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 900,
    "y": 300,
    "wires": [
        ["c6bcde46.c227d"]
    ]
}, {
    "id": "c6bcde46.c227d",
    "type": "ui_led",
    "z": "16dba669.90683a",
    "group": "f6310671.b6a5c8",
    "order": 5,
    "width": "3",
    "height": "1",
    "label": "R1",
    "labelPlacement": "left",
    "labelAlignment": "right",
    "colorForValue": [{
        "color": "red",
        "value": "0",
        "valueType": "str"
    }, {
        "color": "green",
        "value": "1",
        "valueType": "str"
    }],
    "allowColorForValueInMessage": false,
    "name": "",
    "x": 1110,
    "y": 300,
    "wires": []
}, {
    "id": "bd7c47f8.25f1c8",
    "type": "mqtt in",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/R2/read/value",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 900,
    "y": 360,
    "wires": [
        ["5d7bf64e.192868"]
    ]
}, {
    "id": "5d7bf64e.192868",
    "type": "ui_led",
    "z": "16dba669.90683a",
    "group": "f6310671.b6a5c8",
    "order": 6,
    "width": "3",
    "height": "1",
    "label": "R2",
    "labelPlacement": "left",
    "labelAlignment": "right",
    "colorForValue": [{
        "color": "red",
        "value": "0",
        "valueType": "str"
    }, {
        "color": "green",
        "value": "1",
        "valueType": "str"
    }],
    "allowColorForValueInMessage": false,
    "name": "",
    "x": 1110,
    "y": 360,
    "wires": []
}, {
    "id": "a0b31d5.0fd5be",
    "type": "mqtt in",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/R3/read/value",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 900,
    "y": 420,
    "wires": [
        ["c1ea0b7.d07a7f8"]
    ]
}, {
    "id": "c1ea0b7.d07a7f8",
    "type": "ui_led",
    "z": "16dba669.90683a",
    "group": "f6310671.b6a5c8",
    "order": 7,
    "width": "3",
    "height": "1",
    "label": "R3",
    "labelPlacement": "left",
    "labelAlignment": "right",
    "colorForValue": [{
        "color": "red",
        "value": "0",
        "valueType": "str"
    }, {
        "color": "green",
        "value": "1",
        "valueType": "str"
    }],
    "allowColorForValueInMessage": false,
    "name": "",
    "x": 1110,
    "y": 420,
    "wires": []
}, {
    "id": "2e3ba051.2e28d",
    "type": "mqtt in",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/PLUS/read/value",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 910,
    "y": 480,
    "wires": [
        ["7f36a4ba.a833dc"]
    ]
}, {
    "id": "7f36a4ba.a833dc",
    "type": "ui_led",
    "z": "16dba669.90683a",
    "group": "f6310671.b6a5c8",
    "order": 8,
    "width": "3",
    "height": "1",
    "label": "+",
    "labelPlacement": "left",
    "labelAlignment": "right",
    "colorForValue": [{
        "color": "red",
        "value": "0",
        "valueType": "str"
    }, {
        "color": "green",
        "value": "1",
        "valueType": "str"
    }],
    "allowColorForValueInMessage": false,
    "name": "",
    "x": 1110,
    "y": 480,
    "wires": []
}, {
    "id": "f857e352.bd466",
    "type": "mqtt in",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/MINUS/read/value",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 920,
    "y": 540,
    "wires": [
        ["73f55be9.b4c9b4"]
    ]
}, {
    "id": "73f55be9.b4c9b4",
    "type": "ui_led",
    "z": "16dba669.90683a",
    "group": "f6310671.b6a5c8",
    "order": 9,
    "width": "3",
    "height": "1",
    "label": "-",
    "labelPlacement": "left",
    "labelAlignment": "right",
    "colorForValue": [{
        "color": "red",
        "value": "0",
        "valueType": "str"
    }, {
        "color": "green",
        "value": "1",
        "valueType": "str"
    }],
    "allowColorForValueInMessage": false,
    "name": "",
    "x": 1110,
    "y": 540,
    "wires": []
}, {
    "id": "1abadb23.1591c5",
    "type": "mqtt in",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/PREV/read/value",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 910,
    "y": 600,
    "wires": [
        ["4e550c4e.e71b34"]
    ]
}, {
    "id": "4e550c4e.e71b34",
    "type": "ui_led",
    "z": "16dba669.90683a",
    "group": "f6310671.b6a5c8",
    "order": 10,
    "width": "3",
    "height": "1",
    "label": "<<",
    "labelPlacement": "left",
    "labelAlignment": "right",
    "colorForValue": [{
        "color": "red",
        "value": "0",
        "valueType": "str"
    }, {
        "color": "green",
        "value": "1",
        "valueType": "str"
    }],
    "allowColorForValueInMessage": false,
    "name": "",
    "x": 1110,
    "y": 600,
    "wires": []
}, {
    "id": "21df40eb.3f9c8",
    "type": "mqtt in",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/NEXT/read/value",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 910,
    "y": 660,
    "wires": [
        ["1545ccf6.8a2963"]
    ]
}, {
    "id": "1545ccf6.8a2963",
    "type": "ui_led",
    "z": "16dba669.90683a",
    "group": "f6310671.b6a5c8",
    "order": 11,
    "width": "3",
    "height": "1",
    "label": ">>",
    "labelPlacement": "left",
    "labelAlignment": "right",
    "colorForValue": [{
        "color": "red",
        "value": "0",
        "valueType": "str"
    }, {
        "color": "green",
        "value": "1",
        "valueType": "str"
    }],
    "allowColorForValueInMessage": false,
    "name": "",
    "x": 1110,
    "y": 660,
    "wires": []
}, {
    "id": "13ffb04f.b64c8",
    "type": "mqtt in",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/PLAY/read/value",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 910,
    "y": 720,
    "wires": [
        ["636c20cc.49523"]
    ]
}, {
    "id": "636c20cc.49523",
    "type": "ui_led",
    "z": "16dba669.90683a",
    "group": "f6310671.b6a5c8",
    "order": 12,
    "width": "3",
    "height": "1",
    "label": ">",
    "labelPlacement": "left",
    "labelAlignment": "right",
    "colorForValue": [{
        "color": "red",
        "value": "0",
        "valueType": "str"
    }, {
        "color": "green",
        "value": "1",
        "valueType": "str"
    }],
    "allowColorForValueInMessage": false,
    "name": "",
    "x": 1110,
    "y": 720,
    "wires": []
}, {
    "id": "32460e8.6c6bff2",
    "type": "mqtt in",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/UP/read/value",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 140,
    "y": 60,
    "wires": [
        ["e17e2902.8b33e8"]
    ]
}, {
    "id": "e17e2902.8b33e8",
    "type": "ui_led",
    "z": "16dba669.90683a",
    "group": "e7c1d0cf.0c95d",
    "order": 0,
    "width": "3",
    "height": "1",
    "label": "^",
    "labelPlacement": "left",
    "labelAlignment": "right",
    "colorForValue": [{
        "color": "red",
        "value": "0",
        "valueType": "str"
    }, {
        "color": "green",
        "value": "1",
        "valueType": "str"
    }],
    "allowColorForValueInMessage": false,
    "name": "",
    "x": 350,
    "y": 60,
    "wires": []
}, {
    "id": "9eead0f9.c2e54",
    "type": "mqtt in",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/DOWN/read/value",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 160,
    "y": 120,
    "wires": [
        ["204ce8c0.901068"]
    ]
}, {
    "id": "204ce8c0.901068",
    "type": "ui_led",
    "z": "16dba669.90683a",
    "group": "e7c1d0cf.0c95d",
    "order": 0,
    "width": "3",
    "height": "1",
    "label": "v",
    "labelPlacement": "left",
    "labelAlignment": "right",
    "colorForValue": [{
        "color": "red",
        "value": "0",
        "valueType": "str"
    }, {
        "color": "green",
        "value": "1",
        "valueType": "str"
    }],
    "allowColorForValueInMessage": false,
    "name": "",
    "x": 350,
    "y": 120,
    "wires": []
}, {
    "id": "55a03bd6.b71954",
    "type": "mqtt in",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/LEFT/read/value",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 150,
    "y": 180,
    "wires": [
        ["68957e3b.8a99e"]
    ]
}, {
    "id": "68957e3b.8a99e",
    "type": "ui_led",
    "z": "16dba669.90683a",
    "group": "e7c1d0cf.0c95d",
    "order": 0,
    "width": "3",
    "height": "1",
    "label": "<",
    "labelPlacement": "left",
    "labelAlignment": "right",
    "colorForValue": [{
        "color": "red",
        "value": "0",
        "valueType": "str"
    }, {
        "color": "green",
        "value": "1",
        "valueType": "str"
    }],
    "allowColorForValueInMessage": false,
    "name": "",
    "x": 350,
    "y": 180,
    "wires": []
}, {
    "id": "5b9964cd.fce5dc",
    "type": "mqtt in",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/RIGHT/read/value",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 160,
    "y": 240,
    "wires": [
        ["2d569e3d.87e412"]
    ]
}, {
    "id": "2d569e3d.87e412",
    "type": "ui_led",
    "z": "16dba669.90683a",
    "group": "e7c1d0cf.0c95d",
    "order": 0,
    "width": "3",
    "height": "1",
    "label": ">",
    "labelPlacement": "left",
    "labelAlignment": "right",
    "colorForValue": [{
        "color": "red",
        "value": "0",
        "valueType": "str"
    }, {
        "color": "green",
        "value": "1",
        "valueType": "str"
    }],
    "allowColorForValueInMessage": false,
    "name": "",
    "x": 350,
    "y": 240,
    "wires": []
}, {
    "id": "8dc657f6.856678",
    "type": "mqtt in",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/SELECT/read/value",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 160,
    "y": 480,
    "wires": [
        ["a40f3009.9b442"]
    ]
}, {
    "id": "a40f3009.9b442",
    "type": "ui_led",
    "z": "16dba669.90683a",
    "group": "e7c1d0cf.0c95d",
    "order": 0,
    "width": "3",
    "height": "1",
    "label": "Select",
    "labelPlacement": "left",
    "labelAlignment": "right",
    "colorForValue": [{
        "color": "red",
        "value": "0",
        "valueType": "str"
    }, {
        "color": "green",
        "value": "1",
        "valueType": "str"
    }],
    "allowColorForValueInMessage": false,
    "name": "",
    "x": 350,
    "y": 480,
    "wires": []
}, {
    "id": "45ac556.0dde5ac",
    "type": "mqtt in",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/HOME/read/value",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 160,
    "y": 540,
    "wires": [
        ["7bd18164.04213"]
    ]
}, {
    "id": "7bd18164.04213",
    "type": "ui_led",
    "z": "16dba669.90683a",
    "group": "e7c1d0cf.0c95d",
    "order": 0,
    "width": "3",
    "height": "1",
    "label": "Start",
    "labelPlacement": "left",
    "labelAlignment": "right",
    "colorForValue": [{
        "color": "red",
        "value": "0",
        "valueType": "str"
    }, {
        "color": "green",
        "value": "1",
        "valueType": "str"
    }],
    "allowColorForValueInMessage": false,
    "name": "",
    "x": 350,
    "y": 540,
    "wires": []
}, {
    "id": "dc3f5547.8c1df8",
    "type": "mqtt in",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/START/read/value",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 160,
    "y": 600,
    "wires": [
        ["700003b0.6d088c"]
    ]
}, {
    "id": "700003b0.6d088c",
    "type": "ui_led",
    "z": "16dba669.90683a",
    "group": "e7c1d0cf.0c95d",
    "order": 0,
    "width": "3",
    "height": "1",
    "label": "Start",
    "labelPlacement": "left",
    "labelAlignment": "right",
    "colorForValue": [{
        "color": "red",
        "value": "0",
        "valueType": "str"
    }, {
        "color": "green",
        "value": "1",
        "valueType": "str"
    }],
    "allowColorForValueInMessage": false,
    "name": "",
    "x": 350,
    "y": 600,
    "wires": []
}, {
    "id": "f51910b2.5707a",
    "type": "mqtt in",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/L1/read/value",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 140,
    "y": 300,
    "wires": [
        ["de8accb7.48af6"]
    ]
}, {
    "id": "de8accb7.48af6",
    "type": "ui_led",
    "z": "16dba669.90683a",
    "group": "e7c1d0cf.0c95d",
    "order": 0,
    "width": "3",
    "height": "1",
    "label": "L1",
    "labelPlacement": "left",
    "labelAlignment": "right",
    "colorForValue": [{
        "color": "red",
        "value": "0",
        "valueType": "str"
    }, {
        "color": "green",
        "value": "1",
        "valueType": "str"
    }],
    "allowColorForValueInMessage": false,
    "name": "",
    "x": 350,
    "y": 300,
    "wires": []
}, {
    "id": "d9cd63e4.e386d",
    "type": "mqtt in",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/L2/read/value",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 140,
    "y": 360,
    "wires": [
        ["1f9d0632.95b8da"]
    ]
}, {
    "id": "1f9d0632.95b8da",
    "type": "ui_led",
    "z": "16dba669.90683a",
    "group": "e7c1d0cf.0c95d",
    "order": 0,
    "width": "3",
    "height": "1",
    "label": "L2",
    "labelPlacement": "left",
    "labelAlignment": "right",
    "colorForValue": [{
        "color": "red",
        "value": "0",
        "valueType": "str"
    }, {
        "color": "green",
        "value": "1",
        "valueType": "str"
    }],
    "allowColorForValueInMessage": false,
    "name": "",
    "x": 350,
    "y": 360,
    "wires": []
}, {
    "id": "f1b7d86f.de0548",
    "type": "mqtt in",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/L3/read/value",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 140,
    "y": 420,
    "wires": [
        ["a57fbc9.f04d74"]
    ]
}, {
    "id": "a57fbc9.f04d74",
    "type": "ui_led",
    "z": "16dba669.90683a",
    "group": "e7c1d0cf.0c95d",
    "order": 0,
    "width": "3",
    "height": "1",
    "label": "L3",
    "labelPlacement": "left",
    "labelAlignment": "right",
    "colorForValue": [{
        "color": "red",
        "value": "0",
        "valueType": "str"
    }, {
        "color": "green",
        "value": "1",
        "valueType": "str"
    }],
    "allowColorForValueInMessage": false,
    "name": "",
    "x": 350,
    "y": 420,
    "wires": []
}, {
    "id": "c91cf866.302f68",
    "type": "ui_text_input",
    "z": "16dba669.90683a",
    "name": "",
    "label": "",
    "tooltip": "",
    "group": "29ff3b72.3e3cd4",
    "order": 2,
    "width": "0",
    "height": "0",
    "passthru": false,
    "mode": "text",
    "delay": "0",
    "topic": "",
    "x": 500,
    "y": 300,
    "wires": [
        ["41308705.b699e8"]
    ]
}, {
    "id": "41308705.b699e8",
    "type": "mqtt out",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/LCD/write",
    "qos": "",
    "retain": "",
    "broker": "b2bea16b.4dd8d",
    "x": 690,
    "y": 300,
    "wires": []
}, {
    "id": "2714315f.49003e",
    "type": "ui_button",
    "z": "16dba669.90683a",
    "name": "",
    "group": "29ff3b72.3e3cd4",
    "order": 1,
    "width": 0,
    "height": 0,
    "passthru": false,
    "label": "clear",
    "tooltip": "",
    "color": "",
    "bgcolor": "",
    "icon": "",
    "payload": "1",
    "payloadType": "str",
    "topic": "",
    "x": 490,
    "y": 420,
    "wires": [
        ["70db7c2e.8f1754", "7ab2585b.7fcb48"]
    ]
}, {
    "id": "70db7c2e.8f1754",
    "type": "mqtt out",
    "z": "16dba669.90683a",
    "name": "",
    "topic": "controller/LCD/clear",
    "qos": "",
    "retain": "",
    "broker": "b2bea16b.4dd8d",
    "x": 680,
    "y": 420,
    "wires": []
}, {
    "id": "7ab2585b.7fcb48",
    "type": "function",
    "z": "16dba669.90683a",
    "name": "",
    "func": "msg.payload = \"\"\nreturn msg;",
    "outputs": 1,
    "noerr": 0,
    "x": 540,
    "y": 360,
    "wires": [
        ["c91cf866.302f68"]
    ]
}, {
    "id": "596b7aa6.ab4c74",
    "type": "mqtt in",
    "z": "25436c92.733984",
    "name": "",
    "topic": "panTiltSlider/status/grbl/a/R",
    "qos": "2",
    "datatype": "auto",
    "broker": "b2bea16b.4dd8d",
    "x": 400,
    "y": 480,
    "wires": [
        ["59f3f8f4.fba3e8"]
    ]
}, {
    "id": "59f3f8f4.fba3e8",
    "type": "ui_text",
    "z": "25436c92.733984",
    "group": "6c9bc8e8.0ad8d8",
    "order": 7,
    "width": 3,
    "height": 1,
    "name": "",
    "label": "A",
    "format": "{{msg.payload}}",
    "layout": "row-spread",
    "x": 610,
    "y": 480,
    "wires": []
}, {
    "id": "82da6f05.02ffa",
    "type": "change",
    "z": "6a0d6ea1.57141",
    "name": "",
    "rules": [{
        "t": "set",
        "p": "warm",
        "pt": "flow",
        "to": "payload",
        "tot": "msg"
    }],
    "action": "",
    "property": "",
    "from": "",
    "to": "",
    "reg": false,
    "x": 560,
    "y": 340,
    "wires": [
        ["dc2fd41f.453e48"]
    ]
}]
