var mqtt = require("./mqttHandler.js").mqttHandler

const CONTROLLER_BASETOPIC = require("./mqttBasetopics.js").CONTROLLER_BASETOPIC

class lcd {
    constructor() {
        // console.log("New lcd");
    }
    blinkOff() {
        mqtt.publish(CONTROLLER_BASETOPIC + "/LCD/blinkOff", "");
    }
    blinkOn() {
        mqtt.publish(CONTROLLER_BASETOPIC + "/LCD/blinkOn", "");
    }
    home() {
        mqtt.publish(CONTROLLER_BASETOPIC + "/LCD/home", "");
    }
    clear() {
        mqtt.publish(CONTROLLER_BASETOPIC + "/LCD/clear", "");
    }
    setCursor(col, row) {
        mqtt.publish(CONTROLLER_BASETOPIC + "/LCD/setCursor", Buffer.from([col, row, '\n']));
    }
    // cursorFull(){
    // 	mqtt.publish(CONTROLLER_BASETOPIC + "/LCD/cursorFull","");
    // }
    // setBacklight(level){
    // 	mqtt.publish(CONTROLLER_BASETOPIC + "/LCD/setBacklight",level);
    // }
    print(msg) {
        // console.log("print");
        // console.log(msg);
        mqtt.publish(CONTROLLER_BASETOPIC + "/LCD/write", msg);
    }
}


var LCD = new lcd();
module.exports.lcd = LCD;
